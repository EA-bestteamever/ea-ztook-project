/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ztook.domain.Post;
import java.util.Collection;

/**
 *
 * @author Cena
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Feed {
    private Collection<Post> feeds;
    
    
    public Feed() {
    }

    public Feed(Collection<Post> feeds) {
        this.feeds = feeds;
    }

    public Collection<Post> getFeeds() {
        return feeds;
    }

    public void setFeeds(Collection<Post> feeds) {
        this.feeds = feeds;
    }
    
    
}
