/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ztook.domain.User;
import java.util.Collection;

/**
 *
 * @author Candy
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class StookList {
    private Collection<User> stookList;
    
    
    public StookList() {
    }

    public StookList(Collection<User> stookList) {
        this.stookList = stookList;
    }

    public Collection<User> getStookList() {
        return stookList;
    }

    public void setStookList(Collection<User> stookList) {
        this.stookList = stookList;
    }
    
}
