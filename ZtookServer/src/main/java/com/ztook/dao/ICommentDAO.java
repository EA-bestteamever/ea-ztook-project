/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Comment;

/**
 * Interface for Comment DAO
 * @author Candy
 */
public interface ICommentDAO {
    
    public long create(Comment c);
    public void update(Comment c);
    public void delete(Comment c);
    public Comment read(long commentId);
}
