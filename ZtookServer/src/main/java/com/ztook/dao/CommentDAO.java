/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Comment;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class CommentDAO implements ICommentDAO {
    
    private SessionFactory sf;
     
    public CommentDAO(SessionFactory sf){
        this.sf = sf;
    } 

    @Override
    public long create(Comment c) {
        return (Long) sf.getCurrentSession().save(c);
    }

    @Override
    public void update(Comment c) {
        sf.getCurrentSession().saveOrUpdate(c);
    }

    @Override
    public void delete(Comment c) {
        sf.getCurrentSession().delete(c);
    }

    @Override
    public Comment read(long commentId) {
         return (Comment) sf.getCurrentSession().get(Comment.class, commentId);
    }

   
}
