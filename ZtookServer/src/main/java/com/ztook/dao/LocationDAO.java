/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Event;
import com.ztook.domain.Location;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class LocationDAO implements ILocationDAO {
    
    private SessionFactory sf;
    
    public LocationDAO(SessionFactory sf){
        this.sf = sf;
    }

    @Override
    public long create(Location location) {
        return (Long)sf.getCurrentSession().save(location);
    }

    @Override
    public void update(Location location) {
        sf.getCurrentSession().saveOrUpdate(location);
    }

    @Override
    public void delete(Location location) {
        sf.getCurrentSession().delete(location);        
    }

    @Override
    public Location read(long id) {
         return (Location) sf.getCurrentSession().get(Location.class, id);
    }

}
