/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.Collection;

/**
 * Interface for User DAO
 * @author Candy
 */
public interface IUserDAO {
    public long create(User u);
    public void update(User u);
    public void delete(User u);
    public User read(long id);
    public User getByUserName(String username);
    public Collection<Post> getFeeds(long userId);
    public Collection<User> getStookList(long userId);
    public Collection<User> getPopularUser(long userId);
}
