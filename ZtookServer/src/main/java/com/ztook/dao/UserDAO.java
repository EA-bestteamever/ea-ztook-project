/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class UserDAO implements IUserDAO {
    
    private SessionFactory sf;
    
    public UserDAO(SessionFactory sf){
        this.sf = sf;
    }

    @Override
    public long create(User u) {
       return (Long)sf.getCurrentSession().save(u);
       
    }

    @Override
    public void update(User u) {
        sf.getCurrentSession().saveOrUpdate(u);
    }

    @Override
    public void delete(User u) {
          u.setDeleteFlag(true);
          sf.getCurrentSession().saveOrUpdate(u);
    }

    @Override
    public User read(long id) {
        return (User) sf.getCurrentSession().get(User.class, id);
    }
    
   public User getByUserName(String username) {
        User user = null;
        Query query = sf.getCurrentSession().createQuery("from User u where u.emailAddress =:username ");
        query.setParameter("username", username);
        List result = query.list();
        if(!result.isEmpty()){
            user = (User)result.get(0);
        }
        return user;
    }

    @Override
    public Collection<Post> getFeeds(long userId) {
         Query query2 = sf.getCurrentSession().createQuery("select posts from User u join u.ztookingList z join z.spottedList posts where u.id = :userId order by postDate desc");
         query2.setParameter("userId", userId);
         Collection<Post> resultset= query2.list();
         return resultset;
    }
   @Override
    public Collection<User> getStookList(long userId) {
        
         Query query2 = sf.getCurrentSession().createQuery("select stookList from User u join u.ztookingList stookList where u.id = :userId");
         query2.setParameter("userId", userId);
         
         Collection<User> resultset= query2.list();
         return resultset;
    }
    
    @Override
    public Collection<User> getPopularUser(long id) {
        
          Query query = sf.getCurrentSession().createQuery("Select u from Post p join p.ztookList u group by u.id order by count(u.id) DESC");
          query.setFirstResult(0);
          query.setMaxResults(5);
         
          Collection<User> resultset = query.list();
         
         return resultset;
    }
}
