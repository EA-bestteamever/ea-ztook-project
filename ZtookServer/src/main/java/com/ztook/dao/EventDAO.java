/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Event;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class EventDAO implements IEventDAO {
    
    private SessionFactory sf;
    
    public EventDAO(SessionFactory sf){
        this.sf = sf;
    }

    @Override
    public long create(Event e) {
        return (Long) sf.getCurrentSession().save(e);
    }

    @Override
    public void update(Event e) {
        sf.getCurrentSession().saveOrUpdate(e);
    }

    @Override
    public void delete(Event e) {
        sf.getCurrentSession().delete(e);        
    }

    @Override
    public Event read(long id) {
         return (Event) sf.getCurrentSession().get(Event.class, id);
    }

}
