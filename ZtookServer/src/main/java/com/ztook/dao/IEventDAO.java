/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Event;

/**
 *
 * @author Candy
 */
public interface IEventDAO {
    public long create(Event event);
    public void update(Event event);
    public void delete(Event event);
    public Event read(long eventId);
}
