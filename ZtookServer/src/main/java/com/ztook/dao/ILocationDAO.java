/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Location;

/**
 *
 * @author Candy
 */
public interface ILocationDAO {
    
    public long create(Location l);
    public void update(Location l);
    public void delete(Location l);
    public Location read(long id);
    
}
