/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Post;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class PostDAO implements IPostDAO {
    
    private SessionFactory sf;
    
    public PostDAO(SessionFactory sf){
        this.sf = sf;
    }

    @Override
    public long create(Post p) {
          return (Long)sf.getCurrentSession().save(p);
    }

    @Override
    public void update(Post p) {
        sf.getCurrentSession().saveOrUpdate(p);
    }

    @Override
    public void delete(Post p) {
        sf.getCurrentSession().delete(p);
    }

    @Override
    public Post read(long id) {
        return (Post) sf.getCurrentSession().get(Post.class, id);
    }
}
