/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Post;

/**
 * Interface for Post DAO
 * @author Candy
 */
public interface IPostDAO {
    
    public long create(Post p);
    public void update(Post p);
    public void delete(Post p);
    public Post read(long id);
    
}
