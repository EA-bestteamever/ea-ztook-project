/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Group;
import com.ztook.domain.Post;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class GroupDAO implements IGroupDAO {
    
     private SessionFactory sf;
     
     public GroupDAO(SessionFactory sf){
         this.sf = sf;
     }

    @Override
    public long create(Group g) {
         return (Long)sf.getCurrentSession().save(g);
    }

    @Override
    public void update(Group g) {
        sf.getCurrentSession().saveOrUpdate(g);
    }

    @Override
    public void delete(Group g) {
        sf.getCurrentSession().delete(g);
    }

    @Override
    public Group read(long id) {
         return (Group) sf.getCurrentSession().get(Group.class, id);
    }
    
}
