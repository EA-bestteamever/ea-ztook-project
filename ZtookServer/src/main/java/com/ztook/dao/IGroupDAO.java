/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.dao;

import com.ztook.domain.Group;
import com.ztook.domain.Post;
import java.util.Collection;

/**
 * Interface for Group DAO
 * @author Candy
 */
public interface IGroupDAO {
    
    public long create(Group g);
    public void update(Group g);
    public void delete(Group g);
    public Group read(long id);
    //public Collection<Post> getPostList();
}
