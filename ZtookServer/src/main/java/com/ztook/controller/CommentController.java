/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.controller;

import com.ztook.domain.Comment;
import com.ztook.service.ICommentService;
import com.ztook.service.ILocationService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Cena
 */
@RestController
@RequestMapping(value = "/post/{postId}/comment")
public class CommentController {
    private ICommentService commentService;

    public CommentController(ICommentService commentService) {
        this.commentService = commentService;
    }
    
   @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Comment add(@RequestBody Comment comment){
        long commentId = this.commentService.create(comment);
        return this.commentService.getDetails(commentId);
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public @ResponseBody Comment  update(@RequestBody Comment comment){        
        this.commentService.update(comment);
        return this.commentService.getDetails(comment.getId());
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") long id){
        Map<String, Boolean> returnmap =  new HashMap<String, Boolean>(1);
        try{
            this.commentService.delete(id);
            returnmap.put("result", Boolean.TRUE);
        } catch(Exception e){        
            returnmap.put("result", Boolean.FALSE);
        }
        return returnmap;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Comment getDetails(@PathVariable("id") long id){
        return this.commentService.getDetails(id);
    }
    
}
