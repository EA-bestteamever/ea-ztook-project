/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.controller;

import com.ztook.domain.Location;
import com.ztook.service.ILocationService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Cena
 */
@RestController
@RequestMapping(value = "/location")
public class LocationController {
    private ILocationService locationService;

    public LocationController(ILocationService locationService) {
        this.locationService = locationService;
    }
    
   @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Location add(@RequestBody Location location){        
         long id = this.locationService.add(location);
         return this.locationService.getDetails(id);
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public @ResponseBody Location update(@RequestBody Location location){        
        this.locationService.update(location);
         return this.locationService.getDetails(location.getId());
    }
        
     @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") long id){
        Map<String, Boolean> returnmap =  new HashMap<String, Boolean>(1);
        try{
           this.locationService.delete(id);
            returnmap.put("result", Boolean.TRUE);
        } catch(Exception e){        
            returnmap.put("result", Boolean.FALSE);
        }
        return returnmap;
    }
    
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public @ResponseBody Location getDetails(@PathVariable("id") long id){
        Location location = this.locationService.getDetails(id);
        return this.locationService.getDetails(location.getId());
    }
    
}
