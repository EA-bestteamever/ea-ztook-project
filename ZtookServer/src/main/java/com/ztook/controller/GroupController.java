/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.controller;

import com.ztook.domain.Group;
import com.ztook.service.IGroupService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


/**
 *
 * @author Winnie 
 */
@Controller
@RequestMapping(value="/group")
public class GroupController {
    
    private IGroupService groupService;

    public GroupController(IGroupService groupService) {
        this.groupService = groupService;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Group add(@RequestBody Group group){
         long id = this.groupService.add(group);
         return this.groupService.getDetails(id);
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public @ResponseBody Group update(@RequestBody Group group){        
        this.groupService.update(group);
        return this.groupService.getDetails(group.getId());
    }
        
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") long id){
        Map<String, Boolean> returnmap =  new HashMap<String, Boolean>(1);
        try{
            this.groupService.delete(id);
            returnmap.put("result", Boolean.TRUE);
        } catch(Exception e){        
            returnmap.put("result", Boolean.FALSE);
        }
        return returnmap;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Group getDetails(@PathVariable("id") long id){
       return this.groupService.getDetails(id);
    }
}
