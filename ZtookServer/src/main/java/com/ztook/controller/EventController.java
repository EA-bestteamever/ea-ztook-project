/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.controller;

import com.ztook.domain.Event;
import com.ztook.service.IEventService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Cena
 */
@RestController
@RequestMapping(value = "/event")
public class EventController {
    private IEventService eventService;

    public EventController(IEventService eventService) {
        this.eventService = eventService;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody Event add(@RequestBody Event event){
        long eventId = this.eventService.create(event);
        return this.eventService.getDetails(eventId);
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public @ResponseBody Event update(@RequestBody Event event){        
        this.eventService.update(event);
        return this.eventService.getDetails(event.getId());
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") long id){
        Map<String, Boolean> returnmap =  new HashMap<String, Boolean>(1);
        try{
        this.eventService.delete(id);
            returnmap.put("result", Boolean.TRUE);
        } catch(Exception e){        
            returnmap.put("result", Boolean.FALSE);
        }
        return returnmap;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public  @ResponseBody Event getDetails(@PathVariable("id") long id){
        return this.eventService.getDetails(id);
    }
}
