/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.controller;

import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import com.ztook.service.IGroupService;
import com.ztook.service.IUserService;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import util.Feed;
import util.StookList;

/**
 *
 * @author Candy
 */
@Controller
@RequestMapping(value="/user")
public class UserController {
    
    private IUserService userSvc;
    private IGroupService groupSvc;
    
    public UserController(IUserService u,IGroupService g){
        this.userSvc = u;
        this.groupSvc = g;
    }
    
    
    @RequestMapping(value="/validate",method=RequestMethod.POST)
    public @ResponseBody User validate(@RequestBody User user) {
        return this.userSvc.getByUserName(user.getEmailAddress());
    }
    
    @RequestMapping(value="/",method=RequestMethod.POST)
    public @ResponseBody User add(@RequestBody User u) {
            long id = this.userSvc.create(u);
            return this.userSvc.getDetail(id);
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable("id") long id) {
                return this.userSvc.getDetail(id);
    }
    
    @RequestMapping(value="/update",method=RequestMethod.POST)
    public @ResponseBody User update(@RequestBody User userParam) {
		System.out.println("Updating: "+userParam);
                this.userSvc.update(userParam);
                User updatedUser = this.userSvc.getDetail(userParam.getId());
                
                return updatedUser;
    }
        
     @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") long id){
        Map<String, Boolean> returnmap =  new HashMap<String, Boolean>(1);
        try{
             this.userSvc.delete(id);
            returnmap.put("result", Boolean.TRUE);
        } catch(Exception e){        
            returnmap.put("result", Boolean.FALSE);
        }
        return returnmap;
    }
    
    @RequestMapping(value="/{id}/posts",method=RequestMethod.GET)
    public @ResponseBody Collection<Post> getPosts(@PathVariable("id") long id) {
        return this.userSvc.getPosts(id);
    }
    
    @RequestMapping(value="/{id}/groups",method=RequestMethod.GET)
    public @ResponseBody Collection<Group> getGroups(@PathVariable("id") long id) {
        return this.userSvc.getGroupList(id);
    }
    
    @RequestMapping(value="/{id}/spotted",method=RequestMethod.GET)
    public @ResponseBody Feed getSpotted(@PathVariable("id") long id) {        
        return new Feed(this.userSvc.getSpottedList(id));
    }
    
    @RequestMapping(value="/{userid}/group/join/{groupid}",method=RequestMethod.GET)
    public @ResponseBody Collection<Group> joinGroup(@PathVariable("userid") long userid,@PathVariable("groupid") long groupid) {        
        User user = this.userSvc.getDetail(userid);  
        Group group = this.groupSvc.getDetails(groupid);
        user.addGroup(group);
        this.userSvc.update(user);
        return this.userSvc.getGroupList(userid);
    }
    
    @RequestMapping(value="/{userid}/group/leave/{groupid}",method=RequestMethod.GET)
    public @ResponseBody Collection<Group> leaveGroup(@PathVariable("userid") long userid,@PathVariable("groupid") long groupid) {
        User user = this.userSvc.getDetail(userid);  
        Group group = this.groupSvc.getDetails(groupid);
        user.removeGroup(group);
        this.userSvc.update(user);
        return this.userSvc.getGroupList(userid);
    }
    
    @RequestMapping(value="/{userid}/Addstook/{stookid}",method=RequestMethod.GET)
    public @ResponseBody User addZtook(@PathVariable("userid") long userid,@PathVariable("stookid") long stookid) {
       
         User user = this.userSvc.getDetail(userid); 
              User ztook = this.userSvc.getDetail(stookid); 
        try {
             
              user.addZtooking(ztook);
              
        this.userSvc.update(user);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }
        
      
        return ztook;
    }
    
    @RequestMapping(value="/{userid}/stook/remove/{stookid}",method=RequestMethod.GET)
    public@ResponseBody Collection<User> removeStook(@PathVariable("userid") long userid,@PathVariable("stookid") long stookid) {        
        User user = this.userSvc.getDetail(userid); 
        User ztook = this.userSvc.getDetail(stookid); 
        user.removeZtooking(ztook);
        this.userSvc.update(user);
        return this.userSvc.getZtookList(userid);
    }
    
    @RequestMapping(value="/{userid}/feeds",method=RequestMethod.GET)
    public @ResponseBody Feed getFeeds(@PathVariable("userid") long userid) {        
        Collection<Post> post = this.userSvc.getFeeds(userid);
        Feed feed = new Feed(post);
        return feed;
    }
    
    @RequestMapping(value="/{userid}/stookList",method=RequestMethod.GET)
    public @ResponseBody StookList getStookList(@PathVariable("userid") long userid) {        
        Collection<User> stookList = this.userSvc.getZtookList(userid);
        StookList list = new StookList(stookList);
        return list;
    }
    
    @RequestMapping(value="/{userid}/popularUser",method=RequestMethod.GET)
    public @ResponseBody StookList getPopularUser(@PathVariable("userid") long userid) {        
        Collection<User> user = this.userSvc.getPopularUser(userid);
        StookList list = new StookList(user);
        return list;
    }
   
}
