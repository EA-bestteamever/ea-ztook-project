/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Group;

/**
 *
 * @author Candy
 */
public interface IGroupService {
    public long add(Group g);
    public void update(Group g);
    public void delete(long id);
    public Group getDetails(long id);
    
}
