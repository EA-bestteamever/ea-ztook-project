/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Comment;
import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;

/**
 *
 * @author Candy
 */
public interface IZtookService {
    
    public void register(User u);
    
    public void addPost(User u, Post p);
    public void removePost(User u, Post p);
    public void updatePost(Post p);
    
    public void addZtook(User u,User stook);
    public void removeZtook(User u,User stook);
    
    public void joinGroup(User u, Group g);
    public void leaveGroup(User u, Group g);
    
    public void addPostComment(Post p, Comment c);
    public void removePostComment(Post p, Comment c);
    public void updatePostComment(Comment c);
}
