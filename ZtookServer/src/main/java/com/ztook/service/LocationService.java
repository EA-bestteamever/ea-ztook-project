/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.ILocationDAO;
import com.ztook.domain.Location;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class LocationService implements ILocationService {
    
    private ILocationDAO locationDAO;
    
    public LocationService(ILocationDAO l){
        this.locationDAO = l;
    }

    @Override
    public long add(Location l) {
        return this.locationDAO.create(l);
    }

    @Override
    public void update(Location l) {
        this.locationDAO.update(l);
    }

    @Override
    public void delete(long id) {
        Location location = this.getDetails(id);
        this.locationDAO.delete(location);
    }

    @Override
    public Location getDetails(long id) {
        return this.locationDAO.read(id);
    }
    
}
