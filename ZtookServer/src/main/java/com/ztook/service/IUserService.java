/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.Collection;

/**
 *
 * @author Candy
 */
public interface IUserService {
    public long create(User u);
    public void delete(long id);
    public void update(User u);
    public User getDetail(long id);
    public Collection<Post> getPosts(long id);
    public Collection<Post> getSpottedList(long id);
    public Collection<User> getZtookList(long id);
    public Collection<Group> getGroupList(long id);
    public User getByUserName(String username);
    public void addZtook(User u,User stook);
    public Collection<Post> getFeeds(long id);
     public Collection<User> getPopularUser(long id);
}
