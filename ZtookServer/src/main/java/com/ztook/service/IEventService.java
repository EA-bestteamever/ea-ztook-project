/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Event;

/**
 *
 * @author Candy
 */
public interface IEventService {
    public long create(Event e);
    public void update(Event e);
    public void delete(long id);
    public Event getDetails(long id);
}
