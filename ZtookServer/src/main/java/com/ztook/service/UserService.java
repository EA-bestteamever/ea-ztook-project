/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.IUserDAO;
import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.Collection;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRED)
public class UserService implements IUserService {
    
    private IUserDAO userDAO;
    
    public UserService(IUserDAO u){
        this.userDAO = u;
    }

    @Override
    public long create(User u) {
        return this.userDAO.create(u);
    }

    @Override
    public void delete(long id){
        User u = this.getDetail(id);
        this.userDAO.delete(u);
    }

    @Override
    public void update(User u) {
        this.userDAO.update(u);
    }
  
    @Override
    public User getDetail(long id) {
        return this.userDAO.read(id);
    }

    @Override
    public Collection<Post> getPosts(long id) {
        User u = this.getDetail(id);
        return u.getPostList();
    }
    
    @Override
    public Collection<Post> getSpottedList(long id) {
        User u = this.getDetail(id);
        return u.getSpottedList();
    }

    @Override
    public Collection<Group> getGroupList(long id) {
        User u = this.getDetail(id);
        return u.getGroupList();
    }

    @Override
    public Collection<User> getZtookList(long id) {
       return this.userDAO.getStookList(id);
    }

    @Override
    public User getByUserName(String username) {
          return this.userDAO.getByUserName(username);
    }

    @Override
    public void addZtook(User u, User stook) {
        u.addZtooking(stook);
        this.update(u);
    } 
    
    @Override    
    public Collection<Post> getFeeds(long id){
        return this.userDAO.getFeeds(id);
    }

   @Override
    public Collection<User> getPopularUser(long id) {
        return this.userDAO.getPopularUser(id);
    }

}
