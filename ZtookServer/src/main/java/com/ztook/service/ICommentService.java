/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Comment;

/**
 *
 * @author Candy
 */
public interface ICommentService {
    public long create(Comment c);
    public void delete(long id);
    public void update(Comment c);
    public Comment getDetails(long id);
}
