/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.IPostDAO;
import com.ztook.domain.Comment;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.Collection;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class PostService implements IPostService {

    private IPostDAO postDAO;
    
    public PostService(IPostDAO p){
        this.postDAO = p;
    }

    @Override
    public long add(Post p) {
        return this.postDAO.create(p);
    }

    @Override
    public void update(Post p) {
        this.postDAO.update(p);
    }

    @Override
    public void delete(long postId) {
        Post p = this.getDetails(postId);
        this.postDAO.delete(p);
    }
    
    @Override
    public Post getDetails(long postId) {
        return this.postDAO.read(postId);
    }
        
    @Override
    public Collection<Comment> getComments(long postId) {
        Post post = this.getDetails(postId);
        return post.getCommentList();
    }
            
    @Override
    public Collection<User> getZtookList(long postId) {
        Post post = this.getDetails(postId);
        return post.getZtookList();
    }
    
    @Override
    public void addComment(long postId, Comment comment){
        Post post = this.getDetails(postId);
        post.addComment(comment);
        this.update(post);
    }
    
    
    @Override
    public void addZtook(long postId, User user){
        Post post = this.getDetails(postId);
        post.addZtook(user);
        this.update(post);
    }
    
    
}
