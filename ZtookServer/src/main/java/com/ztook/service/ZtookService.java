/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Comment;
import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class ZtookService implements IZtookService {
    
   private IUserService userSvc;
   private IGroupService groupSvc;
   private IPostService postSvc;
   private ICommentService commentSvc;
    
    public ZtookService(IUserService u,IGroupService g, IPostService p, ICommentService c){
        this.userSvc = u;
        this.groupSvc = g;
        this.postSvc = p;
        this.commentSvc = c;
    }

    @Override
    public void addPost(User u, Post p) {
        u.addPost(p);
        this.userSvc.update(u);
    }

    @Override
    public void removePost(User u, Post p) {
       u.removePost(p);
       this.userSvc.update(u);
    }

    @Override
    public void addZtook(User u, User stook) {
        u.addZtooking(stook);
        this.userSvc.update(u);
    }

    @Override
    public void removeZtook(User u, User stook) {
        u.removeZtooking(stook);
        this.userSvc.update(u);
    }

    @Override
    public void joinGroup(User u, Group g) {
        u.addGroup(g);
        this.userSvc.update(u);
    }

    @Override
    public void leaveGroup(User u, Group g) {
        u.removeGroup(g);
        this.userSvc.update(u);
    }

    @Override
    public void updatePost(Post p) {
        this.postSvc.update(p);
    }

    @Override
    public void addPostComment(Post p, Comment c) {
        p.addComment(c);
        this.postSvc.update(p);
    }

    @Override
    public void removePostComment(Post p, Comment c) {
        p.removeComment(c);
        this.postSvc.update(p);
    }

    @Override
    public void updatePostComment(Comment c) {
        this.commentSvc.update(c);
    }

    @Override
    public void register(User u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 
}
