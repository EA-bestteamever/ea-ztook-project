/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Comment;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import java.util.Collection;

/**
 *
 * @author Candy
 */
public interface IPostService {
    
    public long add(Post p);
    public void update(Post p);
    public void delete(long postId);
    public Post getDetails(long postId);
    public Collection<Comment> getComments(long postId);
    public void addComment(long postId, Comment comment);
    public Collection<User> getZtookList(long postId);
     public void addZtook(long postId, User user);
    
}
