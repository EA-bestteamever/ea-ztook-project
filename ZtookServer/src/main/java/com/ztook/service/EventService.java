/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.IEventDAO;
import com.ztook.domain.Event;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class EventService implements IEventService {
    
    private IEventDAO eventDAO;
    
    public EventService(IEventDAO e) {
        this.eventDAO = e;
    }

    @Override
    public long create(Event e) {
        return this.eventDAO.create(e);
    }

    @Override
    public void update(Event e) {
        this.eventDAO.update(e);
    }

    @Override
    public void delete(long id) {
        Event e = this.getDetails(id);
        this.eventDAO.delete(e);
    }

    @Override
    public Event getDetails(long id) {
        return this.eventDAO.read(id);
    }
    
    
}
