/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.domain.Location;

/**
 *
 * @author Candy
 */
public interface ILocationService {
    public long add(Location l);
    public void update(Location l);
    public void delete(long id);
    public Location getDetails(long id);
}
