/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.IGroupDAO;
import com.ztook.domain.Group;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class GroupService implements IGroupService {
    
    private IGroupDAO groupDAO;
    
    public GroupService(IGroupDAO g){
        this.groupDAO = g;
    }

    @Override
    public long add(Group g) {
        return this.groupDAO.create(g);
    }

    @Override
    public void update(Group g) {
        this.groupDAO.update(g);
    }

    @Override
    public void delete(long id) {
        Group g = this.getDetails(id);
        this.groupDAO.delete(g);
    }

    @Override
    public Group getDetails(long id) {
       return this.groupDAO.read(id);
    }
    
}
