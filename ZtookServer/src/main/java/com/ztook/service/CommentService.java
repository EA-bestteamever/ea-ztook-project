/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.service;

import com.ztook.dao.ICommentDAO;
import com.ztook.domain.Comment;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Candy
 */
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class CommentService implements ICommentService {
    
    private ICommentDAO commentDAO;
    
    public CommentService(ICommentDAO c){
        this.commentDAO = c;
    }

    @Override
    public long create(Comment c) {
        return this.commentDAO.create(c);
    }

    @Override
    public void delete(long id) {
        Comment comment = this.getDetails(id);
        this.commentDAO.delete(comment);
    }

    @Override
    public void update(Comment c) {
        this.commentDAO.update(c);
    }

    @Override
    public Comment getDetails(long id) {
        return this.commentDAO.read(id);
    }
    
}
