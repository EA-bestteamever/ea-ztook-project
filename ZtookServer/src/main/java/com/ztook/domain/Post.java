/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author aambil
 */
@Entity
@Table(name="ztookpost")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Post {
    @Id
    @GeneratedValue
//    //@NotEmpty
    private long id;
//    //@NotEmpty
    private String caption;
//    //@NotEmpty
    private String imagePath;
    @Temporal(TemporalType.TIMESTAMP)
    private Date postDate;
    @ManyToOne (cascade = CascadeType.ALL)
    private Event event;
    @ManyToOne (cascade = CascadeType.ALL)
    private Location location;
    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private User postedBy;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "spottedList")
    private List<User> ztookList;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "post")
    private List<Comment> commentList;
    //@NotEmpty
    private boolean authorVisibility;
    //@NotEmpty
    private boolean confirmStatus;
    @ManyToOne
    private Group socialGroup;

    public Post() {
        ztookList = new ArrayList<User>();
        commentList = new ArrayList<Comment>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public User getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(User postedBy) {
        this.postedBy = postedBy;
        this.postedBy.addPost(this);
        
    }

    public List<User> getZtookList() {
        return ztookList;
    }

    public void setZtookList(List<User> ztookList) {
        this.ztookList = ztookList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public boolean isAuthorVisibility() {
        return authorVisibility;
    }

    public void setAuthorVisibility(boolean authorVisibility) {
        this.authorVisibility = authorVisibility;
    }

    public boolean isConfirmStatus() {
        return confirmStatus;
    }

    public void setConfirmStatus(boolean confirmStatus) {
        this.confirmStatus = confirmStatus;
    }

    public Group getSocialGroup() {
        return socialGroup;
    }

    public void setSocialGroup(Group socialGroup) {
        this.socialGroup = socialGroup;
    }
    
     public boolean addZtook(User user){
       return this.ztookList.add(user);
    }
    
     public boolean removeZtook(User user){
       user.removeSpotted(this);
       return this.ztookList.remove(user);
    }
     
     public boolean addComment(Comment comment){
         return this.commentList.add(comment);
     }
     
     public boolean removeComment(Comment comment){
         return this.commentList.remove(comment);
     }     
    
}
