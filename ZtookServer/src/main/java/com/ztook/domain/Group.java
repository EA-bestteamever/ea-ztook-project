/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author aambil
 */
@Entity
@Table(name = "socialGroup")
public class Group {
    @Id
    @GeneratedValue
    //@NotEmpty
    private long id;
    //@NotEmpty
    private String name;
    //@NotEmpty
    private int memberCount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "socialGroup")
    private List<Post> postList;

    public Group() {        
        postList = new ArrayList<Post>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }
    
    public boolean addPost(Post post){
        post.setSocialGroup(this);
        return this.postList.add(post);
    }
    
    public boolean deletePost(Post post){
        post.setSocialGroup(this);
        return this.postList.remove(post);
    }
    
    
    
}
