package com.ztook.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="useraccount")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public  class User implements Serializable{
    @Id
    @GeneratedValue
//    @NotEmpty
    private long id;
//    @NotEmpty
    private String firstName;
//    @NotEmpty
    private String lastName;
//    @NotEmpty @Pattern(regexp="^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{4,8}")
    private String password;
//    @NotEmpty @Email
    private String emailAddress;
    @OneToOne
    private Location address;
//    @NotEmpty
    private int accessLevel;
//    @NotEmpty
    private boolean postPrivacy;
//    @NotEmpty
    private int ztookerCount;
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(name = "ztookingList")
    private List<User> ztookingList;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JsonIgnore
    private List<Post> spottedList;   
//    @NotEmpty
    private double rating;
//    @NotEmpty
    private int ranking;
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL, mappedBy = "postedBy")
//    @JsonIgnore
    private List<Post> postList;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Group> groupList;
    private String avatarPath;
    private String language;
//    @NotEmpty
    private boolean deleteFlag;
    
    public User() {
        ztookingList = new ArrayList<User>();
        spottedList = new ArrayList<Post>();
        postList = new ArrayList<Post>();
        groupList = new ArrayList<Group>();
    }
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public Location getAddress() {
        return address;
    }
    public void setAddress(Location address) {
        this.address = address;
    }
    public int getAccessLevel() {
        return accessLevel;
    }
    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }
    public boolean isPostPrivacy() {
        return postPrivacy;
    }
    public void setPostPrivacy(boolean postPrivacy) {
        this.postPrivacy = postPrivacy;
    }
    public int getZtookerCount() {
        return ztookerCount;
    }
    public void setZtookerCount(int ztookerCount) {
        this.ztookerCount = ztookerCount;
    }
    public List<User> getZtookingList() {
        return ztookingList;
    }
    public void setZtookingList(List<User> ztookingList) {
        this.ztookingList = ztookingList;
    }
    public List<Post> getSpottedList() {
        return spottedList;
    }
    public void setSpottedList(List<Post> spottedList) {
        this.spottedList = spottedList;
    }
    public double getRating() {
        return rating;
    }
    public void setRating(double rating) {
        this.rating = rating;
    }
    public int getRanking() {
        return ranking;
    }
    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
    public List<Post> getPostList() {
        return postList;
    }
    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }
    public List<Group> getGroupList() {
        return groupList;
    }
    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
    public String getAvatarPath() {
        return avatarPath;
    }
    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }
    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }
     public boolean isDeleteFlag() {
        return deleteFlag;
    }
    public void setDeleteFlag(boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    
    public boolean addSpotted(Post post){
       post.addZtook(this);
       return this.spottedList.add(post);
    }
    
    public boolean removeSpotted(Post post){
       return this.spottedList.remove(post);
    }
    
   public boolean addZtooking(User user){
       return this.ztookingList.add(user);
   }
   
   public boolean removeZtooking(User user){
       return this.ztookingList.remove(user);
   }
   
   public boolean addPost(Post post){
       return this.postList.add(post);
   }
   
   public boolean removePost(Post post){
       post.setPostedBy(null);
       return this.postList.remove(post);
   }
   
   public boolean addGroup(Group group){
       return this.groupList.add(group);
   }
      
   public boolean removeGroup(Group group){
       return this.groupList.remove(group);
   }
}
