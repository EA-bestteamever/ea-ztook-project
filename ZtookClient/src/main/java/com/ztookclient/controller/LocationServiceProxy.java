/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.controller;

import com.ztook.domain.Location;
import com.ztook.service.ILocationService;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Winnie Versosa
 */
public class LocationServiceProxy implements ILocationService{

    private static final String locURL = "http://localhost:8080/ZtookServer/ztook/location";
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    @Override
    public long add(Location l) {
        Long id  = restTemplate.postForObject(locURL + "/add", l, Long.class);
        return id;
    }

    @Override
    public void update(Location l) {
        restTemplate.put(locURL + "/update/" + l.getId(), l);
    }

    @Override
    public void delete(long id) {
        restTemplate.delete(locURL + "/delete/" + id);
    }

    @Override
    public Location getDetails(long id) {
        Location l = restTemplate.getForObject(locURL + "/" + id, Location.class);
        return l;
    }
    
}
