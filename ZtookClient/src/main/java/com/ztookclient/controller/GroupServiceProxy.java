/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.controller;

import com.ztook.domain.Group;
import com.ztook.service.IGroupService;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Winnie Versosa
 */
public class GroupServiceProxy implements IGroupService{

    private static final String groupURL = "http://localhost:8080/ZtookServer/ztook/group/";
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    @Override
    public long add(Group g) {
        Long id = restTemplate.postForObject(groupURL + "add", g, Long.class);
        return id;
    }

    @Override
    public void update(Group g) {
        restTemplate.put(groupURL + "update/" + g.getId() , g);
    }

    @Override
    public void delete(long id) {
        restTemplate.delete(groupURL + "delete/" + id);
    }

    @Override
    public Group getDetails(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
