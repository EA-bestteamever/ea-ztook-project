/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ztookclient.controller;

import com.ztookclient.util.FileUploadHandler;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import com.ztook.service.IPostService;
import com.ztook.service.IUserService;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.Calendar;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import util.Feed;
import util.StookList;
import org.springframework.ui.ModelMap;

/**
 *
 * @author Theresa
 */
@Controller
@SessionAttributes("user")
public class ZtookController {
    
        @Resource
        private IUserService userServiceProxy;        
        @Resource
        private IPostService postServiceProxy;
        @Resource
        FileUploadHandler fileUploader;
    
	@RequestMapping("/")
	public String redirectRoot() {

            return "redirect:/profile/";
	}
        
        @RequestMapping(value="/profile/{id}", method=RequestMethod.GET)
	public String getAccount(@PathVariable int id, ModelMap model) {
	    Feed feeds =null;
            model.addAttribute("user", userServiceProxy.getDetail(id));    

            User userProfile = userServiceProxy.getDetail(id);
            User loggedUser = userServiceProxy.getDetail(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());

            StookList popularUser = new StookList(userServiceProxy.getPopularUser(loggedUser.getId()));
            StookList stooks = new StookList(userServiceProxy.getZtookList(id));
                
            String sFound = "0";
            
            if(userProfile.getId() == loggedUser.getId()){
                sFound = "-1";
                feeds = new Feed(userServiceProxy.getFeeds(id));

            } else {
                feeds = new Feed(userServiceProxy.getSpottedList(id));
                
                 for(User u : loggedUser.getZtookingList()){
                    if( u.getId() == userProfile.getId() ){
                         sFound = "1";
                         break;
                    }
                 }
            }

            model.addAttribute("follow", sFound);  
            model.addAttribute("loggedUser", loggedUser);   
            model.addAttribute("user", userProfile);   
            model.addAttribute("feeds", feeds);
            model.addAttribute("popularUsers", popularUser);
            model.addAttribute("stooks", stooks);

            return "mainpage";
	}
        
          @RequestMapping(value="/feedpage", method=RequestMethod.GET)
            public String displayFeed() {
                            return "postentryview";
            }
			
	    @RequestMapping(value = "/image/{filename}", method = RequestMethod.GET)
    public String showImage(@PathVariable String filename, @ModelAttribute("user") User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = user.getId();
        String imagePath = "/ztook/data/userdata/posts";

        // Get requested image by path info.
        String requestedImage = filename + ".jpg";

        System.out.println("REQUESTED IMAGE " + requestedImage + ".jpg");

        // Check if file name is actually supplied to the request URI.
        if (requestedImage == null) {
            // Do your thing if the image is not supplied to the request URI.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return "redirect:/profile/" + id;
        }

        // Decode the file name (might contain spaces and on) and prepare file object.
        File image = new File(imagePath, URLDecoder.decode(requestedImage, "UTF-8"));

        // Check if file actually exists in filesystem.
        if (!image.exists()) {
            // Do your thing if the file appears to be non-existing.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return "redirect:/profile/" + id;
        }

        // Get content type by filename.
        String contentType = request.getServletContext().getMimeType(image.getName());

        // Check if file is actually an image (avoid download of other files by hackers!).
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        if (contentType == null || !contentType.startsWith("image")) {
            // Do your thing if the file appears not being a real image.
            // Throw an exception, or send 404, or show default/warning image, or just ignore it.
            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return "redirect:/profile/" + id;
        }

        // Init servlet response.
        response.reset();
        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(image.length()));
        System.out.println("THIS POINT");
        // Write image content to response.
        Files.copy(image.toPath(), response.getOutputStream());
        return "redirect:/profile/" + id;
    }
		
        
        @RequestMapping(value="/register", method=RequestMethod.POST)
	public String register(User user) {
                System.out.println("FirstName " + user.getFirstName());
                Long id = userServiceProxy.create(user);
                System.out.println("firstname:"+ user.getFirstName());
                
		// return "forward:/j_spring_security_check?j_username=" + user.getEmailAddress()
                //+ "&j_password=" + user.getPassword();
                 
                return  "redirect:/signuplogin.jsp";
                         
	}
        
    @RequestMapping(value = "/spotpost", method = RequestMethod.POST)
    public String spot(Post post, @ModelAttribute("user") User user, @RequestParam("imgInpPost") MultipartFile file, HttpServletRequest request, @RequestParam("ztookeeList") long stokeeid) {

        System.out.println("ZTOOKER " + stokeeid);
        String redirectUrl = "redirect:/profile/";
        String newFilename = "post" + user.getId() + Math.abs(Math.random());
        String rootPath = "/ztook/data/userdata/posts/";

        fileUploader.setFile(file);
        fileUploader.setNewFileName(newFilename);
        fileUploader.setFileSaveDir(rootPath);
        
        User spottedUser = this.userServiceProxy.getDetail(stokeeid);

        spottedUser.addPost(post);
        post.setPostedBy(user);
        post.setPostDate(Calendar.getInstance().getTime());
//        post.addZtook(this.userServiceProxy.getDetail(stokeeid));
        post.setImagePath(fileUploader.getNewFileName());

        if (fileUploader.uploadFile()) {
            System.out.println("File is saved at: " + rootPath + newFilename);
            long id = postServiceProxy.add(post);
            //get user id after sucessful post upload for our redirect url
            redirectUrl += post.getPostedBy().getId();

        } else {
            System.out.println(" Upload failed: " + fileUploader.getNewFileName());
            redirectUrl += post.getPostedBy().getId();
        }
        return redirectUrl;
    }
    
     @RequestMapping(value = "/stookMe/{id}", method = RequestMethod.GET)
        public String stookUser(@ModelAttribute("user") User stook) {
           
            
            User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
           
            this.userServiceProxy.addZtook(loggedUser, stook);
           
            return "redirect:/profile/" + stook.getId();
        }

        @RequestMapping(value = "/stookList/{id}", method = RequestMethod.GET)
        public String getStookUserList(@PathVariable int id, ModelMap model) {
           
           
            User user = userServiceProxy.getDetail(id);
            
            StookList stooks = new StookList(userServiceProxy.getZtookList(id));
            model.addAttribute("stooks", stooks);
            
            User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("loggedUser", loggedUser);
            
            StookList popularUser = new StookList(userServiceProxy.getPopularUser(loggedUser.getId()));
            model.addAttribute("popularUsers", popularUser);
            
             return "mainpage";
        }
    
        
         /**
          * Upload single file using Spring Controller
          */
         @RequestMapping(value = "/uploadFile/{id}", method = RequestMethod.POST)
         public String uploadFileHandler(@PathVariable int id,@RequestParam("fileProfPic") MultipartFile file) {

                fileUploader.setFile(file);
                fileUploader.setNewFileName(id+"");
                fileUploader.setFileSaveDir("/ztook/data/userdata/profile/");

                 try {
                          if (fileUploader.uploadFile()) {
                            User user = this.userServiceProxy.getDetail(id);
                             user.setAvatarPath(fileUploader.getFileAbsolutePath());

                             userServiceProxy.update(user );
                             System.out.println("firstname:"+ user.getFirstName());
                            
                     } 
                  } catch(Exception e){
                         System.out.println("MessageL" + e.getMessage());
                  }
                  return "redirect:/profile/" + id;
         }
}
