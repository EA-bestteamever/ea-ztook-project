/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.controller;

import com.ztook.domain.Comment;
import com.ztook.service.ICommentService;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Winnie Versosa
 */
public class CommentServiceProxy implements ICommentService{
    
    private static final String commentURL = "http://localhost:8080/ZtookServer/ztook/comment";
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    @Override
    public long create(Comment c) {
        Long id = restTemplate.postForObject(commentURL + "/add", c, Long.class);
        return id;
    }

    @Override
    public void delete(long id) {
        restTemplate.delete(commentURL + "/delete/" + id);
    }

    @Override
    public void update(Comment c) {
        restTemplate.put(commentURL + "/update/" + c.getId(), c);
    }

    @Override
    public Comment getDetails(long id) {
        Comment c = restTemplate.getForObject(commentURL + "/" + id, Comment.class);
        return c;
    }
    
}
