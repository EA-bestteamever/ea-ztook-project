/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ztookclient.controller;

import com.ztook.domain.Group;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import com.ztook.service.IUserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import util.Feed;
import util.StookList;

/**
 *
 * @author Theresa
 */
public class UserServiceProxy implements IUserService {

    private static final String userURL = "http://localhost:8080/ZtookServer/ztook/user/";
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public long create(User user) {
        long id = 0L;
        try {
             String password = user.getPassword();
             BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
             String hashedPassword = passwordEncoder.encode(password);
             user.setPassword(hashedPassword);
             User u = restTemplate.postForObject(userURL, user, User.class);
             
             id = u.getId();
        } catch(Exception e){
            System.out.println("Error:" + e.getMessage());
        }
       
        return id;
    }

    @Override
    public void delete(long id) {
        restTemplate.delete(userURL + "/" + id);
    }

    @Override
    public void update(User userParam) {
        this.restTemplate.postForObject(userURL + "update", userParam,User.class);
    }

    //TO DO: Copy from Leah
    @Override
    public User getDetail(long id) {
        String uri = userURL + id;
        return restTemplate.getForObject(uri, User.class);
    }

    @Override
    public Collection<Post> getPosts(long id) {
        String url = userURL+"/"+id+"/posts";
        return restTemplate.getForObject(url, Collection.class);
    }

    @Override
    public Collection<Post> getSpottedList(long id) {
        String url = userURL + "/" + id + "/spotted";
        Feed feed =  restTemplate.getForObject(url, Feed.class);
        return feed.getFeeds();
    }

    @Override
    public Collection<Group> getGroupList(long id) {
        String url = userURL + "/" + id + "/spotted";
        return restTemplate.getForObject(url, Collection.class);
    }    

    @Override
    public Collection<User> getZtookList(long id) {
        Collection<User> stookList = new ArrayList<User>();
        String url = userURL +  id + "/stookList";
        
         try{
           StookList stook =  restTemplate.getForObject(url, StookList.class);
           stookList = stook.getStookList();
           
         } catch(Exception e){
             String message = e.getMessage();
             System.out.println(message);
         }
        return stookList;
    }
    
    @Override
    public User getByUserName(String username){
        User user = new User();
        user.setEmailAddress(username);
        try {
             String str = userURL+"validate";
             user = restTemplate.postForObject(str, user, User.class); 
        } catch(Exception e){
            System.out.println("Error:" + e.getMessage());
        }
        return user;
    }

    @Override
    public Collection<Post> getFeeds(long id) {
       Collection<Post> post = new ArrayList<Post>();
        String url = userURL +  id + "/feeds";
         try{
           Feed feed =  restTemplate.getForObject(url, Feed.class);
           post = feed.getFeeds();
         } catch(Exception e){
             String message = e.getMessage();
             System.out.println(message);
         }
        return post;
    }

    @Override
    public void addZtook(User principal, User stook) {
        this.restTemplate.getForObject(userURL + "/" + principal.getId() 
                + "/Addstook/" + stook.getId(),User.class);
    }
    
   @Override
    public Collection<User> getPopularUser(long id) {
       Collection<User> user = new ArrayList<User>();
        String url = userURL  + id + "/popularUser";
         try{
           StookList feed =  restTemplate.getForObject(url, StookList.class);
           user = feed.getStookList();
         } catch(Exception e){
             String message = e.getMessage();
             System.out.println(message);
         }
        return user;
    }

    
    
}
