/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.controller;

import com.ztook.domain.Comment;
import com.ztook.domain.Post;
import com.ztook.domain.User;
import com.ztook.service.IPostService;
import java.util.Collection;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Winnie Versosa
 */
public class PostServiceProxy implements IPostService {

    public static final String postURL = "http://localhost:8080/ZtookServer/ztook/post/";
    public RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    
    @Override
    public long add(Post p) {
        Long id = restTemplate.postForObject(postURL + "add/", p, Long.class);
        return id.longValue();
    }

    @Override
    public void update(Post p) {
        restTemplate.put(postURL + "update/" + p.getId(), p);
    }

    @Override
    public void delete(long postId) {
        restTemplate.delete(postURL + "delete/" + postId);
    }

    @Override
    public Post getDetails(long postId) {
        Post p = restTemplate.getForObject(postURL + ""+ postId, null);
        return p;
    }

    @Override
    public Collection<Comment> getComments(long postId) {
        Collection c = restTemplate.getForObject(postURL + "" + postId + "/comments", Collection.class);
        return c;
    }

    
    @Override
    public void addComment(long postId, Comment comment) {
        restTemplate.postForObject(postURL + "" + postId, comment, Void.class);
    }

    @Override
    public Collection<User> getZtookList(long postId) {
        Collection c = restTemplate.getForObject(postURL + "" + postId, Collection.class);
        return c;
    }

    @Override
    public void addZtook(long postId, User user) {
        restTemplate.postForObject(postURL + "" + postId + "/ztooklist", user, Void.class);
    }
    
}
