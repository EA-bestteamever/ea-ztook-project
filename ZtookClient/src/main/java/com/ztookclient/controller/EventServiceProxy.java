/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.controller;

import com.ztook.domain.Event;
import com.ztook.service.IEventService;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Winnie 
 */
public class EventServiceProxy implements IEventService{

    private static final String eventURL = "http://localhost:8080/ZtookServer/ztook/event";
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    
    @Override
    public long create(Event e) {
        String url = eventURL + "/add";
        Long id = restTemplate.postForObject(url, e, Long.class);
        return id;
    }

    @Override
    public void update(Event e) {
        restTemplate.put(eventURL + "/update/" + e.getId(), e);
    }

    @Override
    public void delete(long id) {
        restTemplate.delete(eventURL + "/delete/" + id);
    }

//  TO DO: COPY FROM LEAH
    @Override
    public Event getDetails(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
