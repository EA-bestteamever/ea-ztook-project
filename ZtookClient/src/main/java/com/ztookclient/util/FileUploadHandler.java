/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztookclient.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Candy
 */
public class FileUploadHandler {
    private String fileSaveDir;
    private MultipartFile file;
    private String fileAbsolutePath;
    private String newFileName;
    
     
    public FileUploadHandler(String fileSaveDirParam){
        this.fileSaveDir = fileSaveDirParam;
    }

    public String getFileAbsolutePath() {
        return fileAbsolutePath;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public void setFileSaveDir(String fileSaveDir) {
        this.fileSaveDir = fileSaveDir;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public void setFileAbsolutePath(String fileAbsolutePath) {
        this.fileAbsolutePath = fileAbsolutePath;
    }
    
    
   public boolean uploadFile() {
        boolean bOk = false;
        
        if( !this.file.isEmpty() ){
            
            try {
                
                byte[] bytes = file.getBytes();
        
                File dir = createDir();

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                      + File.separator + this.getNewFileName());

                if(serverFile.exists()){ 
                            serverFile.delete();
                }

                BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                this.setFileAbsolutePath(serverFile.getName());
                bOk = true;
                
            } catch(Exception e) {
                System.out.println("Error Message:" + e.getMessage());
            }
        }
        return bOk;
    }
    
    private File createDir(){
        
          File dir = new File(this.fileSaveDir);
             if (!dir.exists())
                  dir.mkdirs();
                
          return dir;
    }
    
    public String getNewFileName(){
        
         String name =  file.getOriginalFilename();
         String fileExtention = (name.indexOf(".") > 0) ? name.substring(name.lastIndexOf('.')) : ".jpg";
               
         return this.newFileName + fileExtention;
    }
    
}
