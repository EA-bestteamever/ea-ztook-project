/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ztookclient.util;

import com.ztook.domain.User;
import com.ztook.service.IUserService;
import com.ztookclient.controller.UserServiceProxy;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Theresa
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {
    @Resource
     private IUserService userServiceProxy;
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication auth = null;
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user = userServiceProxy.getByUserName(name);
                
        if (user!=null && user.getPassword()!=null) {            
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();            
            if(passwordEncoder.matches(password, user.getPassword())){
                List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
                grantedAuths.add((new SimpleGrantedAuthority("ROLE_USER")));
                auth = new UsernamePasswordAuthenticationToken(user, password, grantedAuths);               
            }
        } 
        
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
    
}
