/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ztook.client;

import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Candy
 */
public class UserServiceProxy implements IUserService {
    private RestTemplate restTemplate;
	private String customersURL = "http://localhost:8080/solution25_1-service/rest/customers";
	private String customerURL = "http://localhost:8080/solution25_1-service/rest/customer/{customerNumber}";
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
/*
	public void addCustomer(User user) {
		restTemplate.postForLocation(customersURL, user);
	}

	public void deleteCustomer(String customerNumber) {
		restTemplate.delete(customerURL, customerNumber);
	}

	public Customer getCustomer(String customerNumber) {
		return restTemplate.getForObject(customerURL, Customer.class, customerNumber);
	}

	public Collection<Customer> getCustomers() {
		Customers customers = restTemplate.getForObject(customersURL, Customers.class);
		return customers.getCustomer();
	}

	public void updateCustomer(Customer customer) {
		restTemplate.put(customerURL, customer, customer.getCustomerNumber());
	}
*/
}
