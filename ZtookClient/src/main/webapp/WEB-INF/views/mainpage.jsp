</body>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<html>
  <head>
    <title>Ztook Profile</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Expires" content="0"/>
    <link rel="stylesheet" href="../data/e_f_8_0_c335655102e89a6166592e50af15f667ff71.css" type="text/css"/>    
    <script type="text/javascript"  src="../data/4_b_d_1_f8b8cfb5259b09637d4b431e925a92895d77.js"></script>
    <script type="text/javascript"  src="../js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript"  src="../js/ztook.js"></script>
    </head>

    <body class="preview html-export">

      
<script type="text/javascript"  src="../data/c_d_6_5_907e6b001fe29cdb8820cdbbbc686ff1119a.js"></script>
<link rel="stylesheet" href="../data/3_8_b_9_bff27640c8d9755814c1392ff4e32e7bb40f.css" type="text/css"/>
<script>
  var Preview = new dmsDPPreview_Preview();
</script>

<div id="canvas">
    <div id="canvas-wrapper">
        <div id="canvas-area">
            <div id="main1">
                <div class="ElBox " style="z-index:10001;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:transparent;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:../data/113147_120237_escheresque_ste.png;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:0px;top:0px;width:995px;height:345px;position:absolute;background-image: url(../data/113147_120237_escheresque_ste.png);" id="el2646256_e6e8db99bdd884da899c48bfb717aced" ></div>
    <script>(function() {Preview.mElements.set(2646256, document.getElementById('el2646256_e6e8db99bdd884da899c48bfb717aced'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
    
       <form id="profpicForm" action="/ZtookClient/uploadFile/${user.id}" method="POST" enctype="multipart/form-data">
 
           <label for="fileProfPic" />
	   <input type='file' id="fileProfPic" name="fileProfPic" style="display: none"/>
           
	  <div class="ElImageWrapper " id="el2223556_2a2f6cee740a907315c28102344b76d8" style="z-index:10004;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:597px;top:63px;width:260px;height:245px;position:absolute;" >
            <img id="imgProfPicture" class="ElImage" src="../data/userdata/profile/${user.avatarPath}" alt="image" />../data/userdata/profile/${user.avatarPath}
              
          </div>
            
         <script>
            $("#fileProfPic").change(function(){
                 readURL(this, "imgProfPicture");
                 $("#profpicForm").submit();
            });
         </script>
  </form>             
            <script>(function() {Preview.mElements.set(2223556, document.getElementById('el2223556_2a2f6cee740a907315c28102344b76d8'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:80px;top:118px;z-index:10002;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:109px;position:absolute;" id="el351375_f2b78db2b6e39a647eedd519e22a4917" ><p class="wysiwyg-font-type-Arvo" style="font-size: 84px;"><span style="font-weight: bold; background-color: rgba(186, 140, 132, 0); color: rgb(230, 185, 7);">
                        <a href="../profile/${loggedUser.id}" >Ztook</a></span></p></div>
    <script>(function() {Preview.mElements.set(351375, document.getElementById('el351375_f2b78db2b6e39a647eedd519e22a4917'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElBox " style="z-index:10003;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#5c4221;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:../data/113147_120237_congruent_pentagon_brown.png;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:588px;top:53px;width:279px;height:265px;position:absolute;background-image: url(../data/113147_120237_congruent_pentagon_brown.png);" id="el1986503_181156ef6bdb0ae1738d60fb6750af2d" ></div>
    <script>(function() {Preview.mElements.set(1986503, document.getElementById('el1986503_181156ef6bdb0ae1738d60fb6750af2d'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10006;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:80px;top:223px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:24px;position:absolute;" id="el599320_2999db999b3e7aecc9d917101371c56e" ><p class="wysiwyg-font-type-Arvo"><span style="color: rgb(230, 185, 7);">${user.firstName} ${user.lastName}</span></p></div>
    <div class="ElTextElement " style="z-index:10007;overflow:visible;color:;text-align:right;line-height:normal;font-weight:normal;
         font-style:normal;right:10px;top:10px;font-family:Arial;border-width:0px;border-style:solid;width:990px;
         height:24px;position:relative;" id="welcome_id" ><p class="wysiwyg-font-type-Arvo"><span style="color: rgb(230, 185, 7);">Welcome, ${loggedUser.firstName} ${loggedUser.lastName}! | <a href="../j_spring_security_logout"  style="color: beige" onmouseover="this.style.color='white';" onmouseout="this.style.color='beige';">logout</a></span></p></div>
    <script>(function() {Preview.mElements.set(599320, document.getElementById('el599320_2999db999b3e7aecc9d917101371c56e'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
    <div id="el3552193_2bca4cf64ae4c8ed959df6777d18af2a" class="GeoSVGEl " 
      style="z-index:10009;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;left:892px;top:247px;font-family:;border-width:0px;border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;">
 <svg version="1.2" width="100%" height="100%">
  <ellipse class="shape" cx="50" cy="50" rx="50" ry="50" fill="rgba(194,165,112,0.5)" style="-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.52,0.52); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;" />
 </svg>
</div><script>(function() {Preview.mElements.set(3552193, document.getElementById('el3552193_2bca4cf64ae4c8ed959df6777d18af2a'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
<c:if test="${follow ne -1}" >
<div id="el2323527_fdf33e5bafbe82e17e495022df88c012" class="GeoSVGEl " style="left:892px;top:67px;z-index:10007;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;font-family:;border-width:0px;
     border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;">
    <svg version="1.1" width="100%" height="100%">
  <ellipse class="shape3" cx="50" cy="50" rx="50" ry="50" fill="rgba(194,165,112,0.5)" style="-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%;
           -moz-transform: scale(0.52,0.52); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;" />
 </svg></div>
    <div id="el3781894_e86f857320b912c425f0d2d11b9ea4de" class="IconSVG " style="original_width:32 ;original_height:32 
    ;z-index:10012;color:#000000;rotation:0;current_element_category_id:20881;left:902px;top:77px;current_element_id:1286;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;" >
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
    <html><body><svg version="1.1" x="0px" y="0px" 
    width="100%" height="100%" xml:space="preserve"><g class="shapet" style="-webkit-transform: scale(1, 1);-moz-transform: scale(1, 1);transform: scale(1, 1);"><defs></defs>
                    <a xlink:href="../stookMe/${user.id}">
   <rect display="none" fill="#FFA400" width="32" height="32"></rect><rect id="_x3C_Slice_x3E__109_" display="none" fill="none" width="32" height="32"></rect>
   <path <c:if test="${follow == 1}" >fill='#29cc04'</c:if> fill-rule="evenodd" clip-rule="evenodd" 
         d="M0,15v2c8,12,24,12,32,0v-2C24,3,8,3,0,15z M16,24c-4.418,0-8-3.582-8-8 s3.582-8,8-8c4.418,0,8,3.582,8,8S20.418,24,16,24z M16,11.5c0-0.519,0.105-1.009,0.268-1.473C16.177,10.023,16.092,10,16,10 c-3.313,0-6,2.687-6,6s2.686,6,6,6c3.314,0,6-2.687,6-6c0-0.092-0.022-0.177-0.027-0.267C21.509,15.895,21.019,16,20.5,16 C18.015,16,16,13.985,16,11.5z">
    </path></g></svg></body></html>
</div>
  </c:if>          
<script>(function() {Preview.mElements.set(2323527, document.getElementById('el2323527_fdf33e5bafbe82e17e495022df88c012'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();
</script>
<div id="el4075710_b0056b1a74ae302d89d9e0954ec15c1e" class="GeoSVGEl " style="left:892px;top:159px;z-index:10008;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;font-family:;border-width:0px;
     border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;">
 <svg version="1.2" width="100%" height="100%">
  <ellipse class="shape" cx="50" cy="50" rx="50" ry="50" fill="rgba(194,165,112,0.5)" style="-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.52,0.52);
           -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;" />
 </svg>
</div><script>(function() {Preview.mElements.set(4075710, document.getElementById('el4075710_b0056b1a74ae302d89d9e0954ec15c1e'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
<div id="el3902599_f91801ebe8b82889fe31f13cbe60b175" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10010;color:#000000;rotation:0;current_element_category_id:20973;left:902px;top:256px;current_element_id:1592;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><path fill='#000000' fill="#343434" d="M240.16,53.151c4.364,0,8.09,1.615,11.193,4.846c3.103,3.258,4.646,7.056,4.646,11.391v149.33 c0,4.336-1.544,8.104-4.646,11.25c-3.104,3.145-6.829,4.703-11.193,4.703H15.968c-4.35,0-8.104-1.559-11.25-4.703 C1.573,226.82,0,223.053,0,218.717V69.387c0-4.335,1.573-8.132,4.718-11.391c3.145-3.23,6.9-4.846,11.25-4.846h50.849l8.047-17.398 c1.672-3.967,4.732-7.367,9.167-10.201c4.449-2.805,8.854-4.222,13.204-4.222h61.531c4.35,0,8.756,1.417,13.205,4.222 c4.435,2.833,7.537,6.234,9.294,10.201l7.92,17.398H240.16L240.16,53.151z M128.064,210.811c9.082,0,17.696-1.785,25.842-5.299 s15.217-8.301,21.182-14.365c5.979-6.064,10.725-13.148,14.238-21.195s5.285-16.719,5.285-25.955c0-9.096-1.771-17.739-5.285-25.928 c-3.514-8.188-8.26-15.272-14.238-21.251c-5.965-5.979-13.035-10.711-21.182-14.253c-8.146-3.514-16.761-5.271-25.842-5.271 c-9.067,0-17.709,1.757-25.899,5.271c-8.203,3.542-15.287,8.274-21.252,14.253c-5.979,5.979-10.725,13.035-14.238,21.195 c-3.528,8.133-5.285,16.803-5.285,25.985c0,9.236,1.757,17.908,5.285,25.955c3.514,8.047,8.26,15.131,14.238,21.195 c5.965,6.064,13.049,10.852,21.252,14.365C110.354,209.025,118.997,210.811,128.064,210.811z M128.064,98.687 c6.305,0,12.184,1.19,17.653,3.542c5.455,2.38,10.229,5.582,14.296,9.663c4.08,4.08,7.296,8.869,9.663,14.366 c2.352,5.526,3.541,11.418,3.541,17.739c0,6.29-1.189,12.156-3.541,17.625c-2.367,5.469-5.583,10.285-9.663,14.451 c-4.066,4.164-8.869,7.424-14.366,9.803c-5.512,2.354-11.377,3.514-17.583,3.514c-6.291,0-12.199-1.16-17.709-3.514 c-5.512-2.379-10.3-5.639-14.367-9.803c-4.081-4.166-7.296-9.012-9.663-14.508c-2.352-5.525-3.542-11.393-3.542-17.568 c0-6.32,1.19-12.213,3.542-17.739c2.366-5.497,5.582-10.286,9.663-14.366c4.066-4.081,8.855-7.282,14.367-9.663 C115.865,99.876,121.773,98.687,128.064,98.687z" class="shape" style="-webkit-transform: scale(0.125, 0.125);-moz-transform: scale(0.125, 0.125);transform: scale(0.125, 0.125);"></path></svg></body></html>

</div><script>(function() {Preview.mElements.set(3902599, document.getElementById('el3902599_f91801ebe8b82889fe31f13cbe60b175'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script><div id="el494362_ce373ea5d85d5cd67cccf8953fb89394" class="IconSVG " style="original_width:26 ;original_height:26 
;z-index:10011;color:#000000;rotation:0;current_element_category_id:20881;left:902px;top:169px;current_element_id:1252;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" 
                 width="100%" height="100%" xml:space="preserve"><g class="shape" style="-webkit-transform: scale(1.2307692307692, 1.2307692307692);-moz-transform: scale(1.2307692307692, 1.2307692307692);transform: scale(1.2307692307692, 1.2307692307692);"><defs></defs>
                <rect display="none" fill="#FFA400" width="26" height="26"></rect><rect id="_x3C_Slice_x3E__109_" display="none" fill="none" width="26" height="26">
                </rect><path fill='#000000' fill-rule="evenodd" clip-rule="evenodd" d="M22.703,13.191l3.294-1.374l-1.004-4.225L21.3,7.988 c-0.373-0.629-0.805-1.203-1.29-1.717l1.364-3.315l-3.696-2.28l-2.306,2.867c-0.799-0.21-1.622-0.314-2.454-0.319l-1.445-3.219 L7.277,1.132L7.77,4.693C7.19,5.053,6.659,5.462,6.186,5.91L2.999,4.597L0.72,8.295l2.743,2.218c-0.2,0.755-0.318,1.538-0.335,2.327 l-3.124,1.298l1.007,4.23l3.487-0.375c0.403,0.7,0.875,1.334,1.422,1.893l-1.285,3.116L8.33,25.28l2.216-2.75 c0.752,0.187,1.518,0.291,2.3,0.299l1.312,3.167l4.223-1.007L18,21.396c0.653-0.398,1.255-0.854,1.773-1.368l3.237,1.334 l2.277-3.701l-2.865-2.312C22.595,14.644,22.692,13.92,22.703,13.191z M13,19.014c-3.308,0-5.989-2.692-5.989-6.014 S9.692,6.986,13,6.986S18.989,9.679,18.989,13S16.308,19.014,13,19.014z M13,10.99c-1.11,0-2.01,0.9-2.01,2.01 c0,1.11,0.9,2.01,2.01,2.01s2.01-0.899,2.01-2.01C15.01,11.891,14.11,10.99,13,10.99z"></path></g></svg></body></html>

</div><script>(function() {Preview.mElements.set(494362, document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>
<script>(function() {Preview.mElements.set(3781894, document.getElementById('el3781894_e86f857320b912c425f0d2d11b9ea4de'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10018;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:867px;top:353px;font-family:Arial;border-width:0px;border-style:solid;width:118px;height:24px;position:absolute;" id="el149217_3a3835ba8080cc1d116864bdcdf6cbba" ><p class="wysiwyg-font-type-Arvo">Ztookers (10)</p></div>
    <script>(function() {Preview.mElements.set(149217, document.getElementById('el149217_3a3835ba8080cc1d116864bdcdf6cbba'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:15px;top:353px;z-index:10013;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:117px;height:24px;position:absolute;" id="el3527237_4ba106ad60a51621ef06489cce1e4970" ><p class="wysiwyg-font-type-Arvo">Social Groups</p></div>
    <script>(function() {Preview.mElements.set(3527237, document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:155px;top:353px;z-index:10014;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:125px;height:24px;position:absolute;" id="el2437029_ff953cb804f1b5813828f0f08268d684" ><p class="wysiwyg-font-type-Arvo">Ztooker's Feeds</p></div>
    <script>(function() {Preview.mElements.set(2437029, document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:313px;top:353px;z-index:10015;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:77px;height:24px;position:absolute;" id="el3255003_00b5c69262da1441fb959009bd42a366" ><p class="wysiwyg-font-type-Arvo">My Posts</p></div>
    <script>(function() {Preview.mElements.set(3255003, document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:415px;top:353px;z-index:10016;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:115px;height:24px;position:absolute;" id="el1361103_a1a799bf85dccc7ca1355bcd7243cc02" ><p class="wysiwyg-font-type-Arvo">Spotted Feeds</p></div>
    <script>(function() {Preview.mElements.set(1361103, document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:740px;top:353px;z-index:10017;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:108px;height:24px;position:absolute;" id="el2827483_a5515c27331ed39e873ae54a81befe9b" ><p class="wysiwyg-font-type-Arvo">Ztooking (21)</p></div>
    <script>(function() {Preview.mElements.set(2827483, document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b'), {"interactive":true,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElBox " style="left:0px;top:345px;z-index:10005;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(191,147,77,0.75);background-image:../data/113147_120237_congruent_pentagon_brown.png;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;width:995px;height:40px;position:absolute;background-image: url(../data/113147_120237_congruent_pentagon_brown.png);" id="el1684143_e8bde2dd80d8867fb59b67cb4eb01fe5" ></div>
    <script>(function() {Preview.mElements.set(1684143, document.getElementById('el1684143_e8bde2dd80d8867fb59b67cb4eb01fe5'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    
    <div class="ElGroup " style="z-index:10021;left:705px;top:385px;font-family:;border-width:0px;border-style:solid;width:17px;height:656px;position:absolute;" id="el2460782_ce4085383ba5a61b6a256b3d330c0631" >
      <div id="el762864_e373c0af22de2c99a7fdfca2b563125d" class="GeoSVGEl " style="z-index:10020;color:rgba(191,147,77,0.75);stroke:3;element:ellipse;left:0px;top:639px;font-family:;border-width:0px;border-style:solid;width:17px;height:17px;position:absolute;-webkit-transform-origin: 8.5px 8.5px;-moz-transform-origin: 8.5px 8.5px;-ms-transform-origin: 8.5px 8.5px;-o-transform-origin: 8.5px 8.5px;">
 <svg version="1.2" width="100%" height="100%">
  <ellipse class="shape" cx="50" cy="50" rx="50" ry="50" fill="rgba(191,147,77,0.75)" style="-webkit-transform: scale(0.17,0.17); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.17,0.17); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.17,0.17); -ms-transform-origin: 0% 0%; -o-transform: scale(0.17,0.17); -o-transform-origin: 0% 0%;" />
 </svg>
</div>
    <script>(function() {Preview.mElements.set(762864, document.getElementById('el762864_e373c0af22de2c99a7fdfca2b563125d'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    
    <div class="ElBox " style="z-index:10019;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(191,147,77,0.75);background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:url('');padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:6px;top:0px;width:5px;height:640px;position:absolute;" id="el2585818_c7c7d20a83199d11fb839c17abbb0c34" ></div>
    <script>(function() {Preview.mElements.set(2585818, document.getElementById('el2585818_c7c7d20a83199d11fb839c17abbb0c34'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    
    </div>
    <script>(function() {Preview.mElements.set(2460782, document.getElementById('el2460782_ce4085383ba5a61b6a256b3d330c0631'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
    <div class="ElGroup" style="left:14px;top:400px;z-index:10022;border-width: 0px;border-color:rgba(222,218,217,0.5);border-style:solid;background-color:rgba(235,235,235,0);
                                             background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;
                                             font-style:normal;font-family:Arial;text-align:center;vertical-align: top;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;width:680px;position:relative;"
                                             id="1" >
         <c:forEach  items="${feeds.feeds}"  var="feed"> 
             <div class="ElBox" style="min-height: 760px;border-color:rgba(222,218,217,0.5);border-style:solid;border-width: 1px;vertical-align: top;position: relative" id="12">
                    <div class="ElTextElement " style="text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px; top:15px;vertical-align: top;
                       font-family:Arial;border-width:0px;border-style:solid;position:absolute;">
                        <p class="wysiwyg-font-type-Cabin"><span style="font-weight: bold;">Spotted! </span><a href="../profile/${feed.ztookList[0].id}" style="color: #fe00ee" onmouseover="this.style.color='pink';" onmouseout="this.style.color='#fe00ee';">${feed.ztookList[0].firstName} ${feed.ztookList[0].lastName}</a> <c:if test="${not empty feed.location.city or not empty feed.event.id }">is </c:if><c:if test="${not empty feed.location.city}">at <span style="color: rgb(0,0,255);">${feed.location.city}</span> </c:if><c:if test="${not empty feed.event.id}">attending <span style="color: rgb(47,176,0);">${feed.event.name}</span></c:if></p> 
                    </div>
                    <textarea class="ElTextarea " disabled style="text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px; top:40px;vertical-align: top;
                       border-width: 0px; border-color:#c8c8c8;border-style:solid;background-color:rgba(255,255,255,0);color:#474747;font-size:12px;
                              font-family:Verdana;max-width: 620px;width:620px;min-height: 50px;position:absolute;overflow:hidden;">${feed.caption}
                   </textarea>
                    <div class="ElImageWrapper" style="z-index:10003;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px
                         border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;
                         left:25px;top:100px;position:absolute;" >
                    <img class="ElImage " style="max-width: 620px;max-height: 620px;border-width:0px;border-style:solid" src="${pageContext.request.contextPath}/image/${feed.imagePath}" />
                    </div>
                <div class="ElTextElement " style="z-index:10004;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:722px;;
                     font-family:Arial;border-width:0px;border-style:solid;width:300px;height:19px;position:relative;" id="el4952186_06f7d26e769142615f2080e2099292f3" >
                    <p class="wysiwyg-font-type-Cabin" style="font-size: 11px;"><span style="color: rgb(135, 135, 135);">posted by Anonymous<%--<c:out value="${feed.postedBy.firstName}"/>--%> on <fmt:formatDate pattern="yyyy-MM-dd" value="${feed.postDate}"/></span></p>
                </div>
                <script>(function() {Preview.mElements.set(4952186, document.getElementById('el4952186_06f7d26e769142615f2080e2099292f3'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();
                </script>
                <script>(function() {Preview.mElements.set(5146437, document.getElementById('el5146437_40520f195f4e82f7307263dfcdd2f69a'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();
                </script>
                <script>
                Interactions.ori_styles = {"3317655":{"opacity":null,"width":300,"height":43,"top":15,"left":25},"754224":{"opacity":null,"width":335,"height":100,"top":65,"left":25},"4997273":{"opacity":null,"width":335,"height":382,"top":173,"left":25},"4952186":{"opacity":null,"width":300,"height":19,"top":560,"left":25},"5146437":{"opacity":null,"width":32,"height":32,"top":15,"left":325,"original_width":"256 ","original_height":"256 \n"}};
                Interactions.scrollable_el = document.getElementById('');
                </script>
                </div>
                <br>
           </c:forEach>  
                 
           <!-- STOOKING LIST -->
           <c:forEach  items="${stooks.stookList}"  var="stook"> 
                 <div class="ElBox " style="position: relative;height:500px;">
                    <div class="ElTextElement " style="z-index:10001;overflow:visible:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:15px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:43px;position:absolute;" id="el3317655_748af80fc7c9c9f4d197422f1cf207e8" >
                        <p class="wysiwyg-font-type-Cabin"><a href="../profile/${stook.id}"> <c:out value="${stook.firstName} ${stook.lastName}"/>  </a>
                    </p>
                    </div>
                    <div class="ElImageWrapper " id="el4997273_9070913941971be87d14e1eb3f273386" style="z-index:10003;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:25px;top:70px;width:335px;height:382px;position:absolute;" >
                    <img class="ElImage " src="../data/${stook.avatarPath}" />
                    </div>
                 </div>
           </c:forEach> 
    </div>
    <script>(function() {Preview.mElements.set(498096, document.getElementById('el498096_12c2dc7a6ead1e01c31e0dfbf5c9854d'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10026;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:735px;top:830px;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;" id="el2673401_2ca92c4be07e60afb16bed56567f3846" ><p class="wysiwyg-font-type-Arvo">Popular Events</p></div>
    <script>(function() {Preview.mElements.set(2673401, document.getElementById('el2673401_2ca92c4be07e60afb16bed56567f3846'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:735px;top:400px;z-index:10024;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;" id="el2918070_6ef0a4dbccc5e59b64eb97d65499d911" ><p class="wysiwyg-font-type-Arvo">Popular People</p></div>
    <script>(function() {Preview.mElements.set(2918070, document.getElementById('el2918070_6ef0a4dbccc5e59b64eb97d65499d911'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:735px;top:616px;z-index:10025;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;" id="el5389848_20a4ea4f5585b7cd042bb80ec2e6d3c6" ><p class="wysiwyg-font-type-Arvo">Popular Locations</p></div>
    <script>(function() {Preview.mElements.set(5389848, document.getElementById('el5389848_20a4ea4f5585b7cd042bb80ec2e6d3c6'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10041;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:761px;top:985px;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el579276_4ff314eb2878325edb85be15e133f15f" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Event 5</span></p></div>
    <script>(function() {Preview.mElements.set(579276, document.getElementById('el579276_4ff314eb2878325edb85be15e133f15f'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:860px;z-index:10037;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el5898848_3d582086cfcc42bd9d3dee0c9f5ce397" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Event 1</span></p></div>
    <script>(function() {Preview.mElements.set(5898848, document.getElementById('el5898848_3d582086cfcc42bd9d3dee0c9f5ce397'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:890px;z-index:10038;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el5683157_b65ff1b1b9ad5f1b47ba14d0d7d1b479" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Event 2</span></p></div>
    <script>(function() {Preview.mElements.set(5683157, document.getElementById('el5683157_b65ff1b1b9ad5f1b47ba14d0d7d1b479'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:920px;z-index:10039;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el41172_97c992339c85a17987109ec1b3af5e29" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Event 3</span></p></div>
    <script>(function() {Preview.mElements.set(41172, document.getElementById('el41172_97c992339c85a17987109ec1b3af5e29'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:950px;z-index:10040;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el1940600_5fe723a0a4155d93373982723be6af57" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Event 4</span></p></div>
    
    
   
     <!-- Popular LIST -->
     <c:set var="count" value="435" scope="page" />
           <c:forEach  items="${popularUsers.stookList}"  var="popular"> 
               
                 <div class="ElTextElement " style="left:761px;top:${count}px;z-index:10030;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el2045763_13dddf304bae630307de02963eec14d2" >
                     <p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);"><a href="../profile/${popular.id}">${popular.firstName}  ${popular.lastName} </a> </span></p>
				 </div>
                 <c:set var="count" value="${count + 30}" scope="page"/>
           </c:forEach> 

    <script>(function() {Preview.mElements.set(7472692, document.getElementById('el7472692_9deaec57a28ec20a5a7bbe3546db38a7'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:771px;z-index:10036;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el57203_f97595c00714544826da9a2b33add28b" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Location 5</span></p></div>
    <script>(function() {Preview.mElements.set(57203, document.getElementById('el57203_f97595c00714544826da9a2b33add28b'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:676px;z-index:10033;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el1275685_57d9903f3aaa05fde4a91089036e0d58" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Location 2</span></p></div>
    <script>(function() {Preview.mElements.set(1275685, document.getElementById('el1275685_57d9903f3aaa05fde4a91089036e0d58'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:736px;z-index:10035;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el1322238_9a05adad111aee0baaf83ace50d23050" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Location 4</span></p></div>
    <script>(function() {Preview.mElements.set(1322238, document.getElementById('el1322238_9a05adad111aee0baaf83ace50d23050'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:706px;z-index:10034;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el4702536_f7127be3da5231a9c70a4c05126c4566" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Location 3</span></p></div>
    <script>(function() {Preview.mElements.set(4702536, document.getElementById('el4702536_f7127be3da5231a9c70a4c05126c4566'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:761px;top:646px;z-index:10032;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;" id="el6249759_28467af9a8b88c14d614a9b993bb2dfc" ><p class="wysiwyg-font-type-Asap" style="font-size: 14px;"><span style="color: rgb(130, 88, 29);">Location 1</span></p></div>
    <script>(function() {Preview.mElements.set(6249759, document.getElementById('el6249759_28467af9a8b88c14d614a9b993bb2dfc'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElGroup " style="z-index:10055;left:125px;top:430px;font-family:;border-width:0px;border-style:solid;visibility:hidden;width:625px;height:495px;position:absolute;" id="el1203718_4b6d07e448d9a9b4f63597e4756327aa" >
    <form id="postForm" method="post" action="<c:url value='/spotpost' />" role="form" enctype="multipart/form-data">
        <label for="imgInpPost" />
        <input type="file" id="imgInpPost" name="imgInpPost" style="display: none" />
        <div class="ElImageWrapper " id="el454048_70269a41a72a4106ba1819478b3b06f2" style="z-index:10043;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:20px;top:56px;width:300px;height:353px;position:absolute;" >
            <img class="ElImage " id="imgPostPicture" src="${pageContext.request.contextPath}/image/${filename}" />
        </div>
    <script>
          $("#imgInpPost").change(function(){
            readURL(this, "imgPostPicture");
          });
    </script>
         

    <script>(function() {Preview.mElements.set(454048, document.getElementById('el454048_70269a41a72a4106ba1819478b3b06f2'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10045;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:335px;top:56px;font-family:Arial;border-width:0px;border-style:solid;width:50px;height:21px;position:absolute;" id="el810973_ee60e8abba36ae95d3d7f5728ff344f1" ><p style="font-family: Asap; text-align: start; line-height: normal; font-size: 14px;"><span style="font-style: normal; color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0); font-weight: bold;">Caption</span></p></div>
    <script>(function() {Preview.mElements.set(810973, document.getElementById('el810973_ee60e8abba36ae95d3d7f5728ff344f1'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script><textarea name="caption" style="z-index:10044;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#ffffff;color:#474747;font-size:12px;font-family:Arial;text-align:left;background-image:url('');font-weight:regular;line-height:normal;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-o-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-moz-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-webkit-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);left:335px;top:80px;width:267px;height:187px;position:absolute;" id="el928834_913bbc476a55b5118b4b01aa7d971d48" class="ElTextarea " ></textarea><script>(function() {Preview.mElements.set(928834, document.getElementById('el928834_913bbc476a55b5118b4b01aa7d971d48'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script><div id="el743961_08eca32de01c5c1990214fb701285d8e" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10051;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:335px;top:289px;font-family:;border-width:0px;border-style:solid;width:18px;height:21px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><g class="shape" style="-webkit-transform: scale(0.0703125, 0.08203125);-moz-transform: scale(0.0703125, 0.08203125);transform: scale(0.0703125, 0.08203125);"><path fill='rgba(135,76,16,0.6)' fill="#343434" d="M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128z M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128z M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128z"></path><path fill='rgba(135,76,16,0.6)' fill="#343434" d="M165.665,128.566c2.933,0.467,7.132,1,12.597,1.666c5.398,0.633,11.064,1.334,16.996,2.1 c5.865,0.767,11.33,1.566,16.462,2.366c5.132,0.833,8.798,1.566,11.13,2.199c3.8,1.101,7.666,3.399,11.531,6.865 c3.865,3.499,7.398,7.531,10.664,12.097s5.932,9.298,7.931,14.229c1.999,4.866,2.999,9.331,2.999,13.396v63.184 c-1.066,0.467-2.266,1.2-3.666,2.3c-1.332,1.1-2.799,2.166-4.398,3.199s-3.065,1.933-4.465,2.699 c-1.467,0.766-2.666,1.133-3.6,1.133H16.021c-3.833,0-6.698-1.166-8.665-3.532c-1.966-2.333-4.399-4.266-7.332-5.799v-63.184 c0-4.065,1-8.53,3-13.396c2.033-4.932,4.632-9.597,7.864-14.063c3.199-4.465,6.765-8.497,10.697-12.096 c3.899-3.566,7.765-5.932,11.597-7.032c1.933-0.633,5.532-1.366,10.764-2.199c5.232-0.8,10.831-1.6,16.762-2.366 c5.932-0.766,11.597-1.467,17.062-2.1c5.432-0.666,9.631-1.199,12.563-1.666c-9.998-6.398-17.862-14.763-23.594-25.06 c-5.731-10.297-8.564-21.494-8.564-33.591c0-9.564,1.833-18.595,5.532-27.026c3.699-8.431,8.698-15.829,14.963-22.228 c6.265-6.332,13.596-11.397,22.061-15.096C109.163,1.866,118.161,0,127.758,0c9.597,0,18.628,1.866,27.126,5.565 c8.514,3.699,15.912,8.764,22.311,15.096c6.332,6.398,11.33,13.796,14.996,22.228s5.465,17.462,5.465,27.026 c0,11.897-2.799,23.027-8.464,33.425C183.527,113.737,175.662,122.168,165.665,128.566z"></path></g></svg></body></html>

</div><script>(function() {Preview.mElements.set(743961, document.getElementById('el743961_08eca32de01c5c1990214fb701285d8e'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">

	<input type="hidden" id="ztookee" name="ztookee" />
	<select name="ztookeeList" class="" style="z-index:10050;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-size:auto;background-repeat:repeat;background-position:center center;background-image:url('');box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;left:365px;top:284px;width:237px;height:30px;position:absolute;">
                <option value="">Who is in this picture?</option>
                <c:forEach  items="${stooks.stookList}"  var="stook"> 
                    <option value=${stook.id}>${stook.firstName} ${stook.lastName}</option>
                </c:forEach>
	</select>
	
    </div>
    <script>(function() {Preview.mElements.set(672685, document.getElementById('el672685_fb5d0aadefe3fe49a7b9cf0f1edbb3a0'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script><div id="el1059797_58f7675a995005ba58c0dd123efbdb21" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10052;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:331px;top:335px;font-family:;border-width:0px;border-style:solid;width:26px;height:23px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><path fill='rgba(135,76,16,0.6)' fill="#343434" d="M218.354,90.682c0,8.954-1.307,17.483-3.921,25.554c-2.614,8.072-6.111,15.718-10.49,22.973l-60.716,105.909 c-4.379,7.255-9.444,10.882-15.195,10.882c-5.784,0-10.751-3.627-14.901-10.882L52.089,138.915 c-4.379-7.288-7.875-14.902-10.489-22.94c-2.647-8.006-3.954-16.437-3.954-25.293c0-12.483,2.353-24.28,7.059-35.325 c4.706-11.045,11.176-20.686,19.41-28.92c8.202-8.202,17.843-14.673,28.888-19.378C104.047,2.353,115.811,0,128.164,0 c12.515,0,24.247,2.353,35.162,7.059c10.979,4.705,20.521,11.176,28.69,19.378c8.171,8.234,14.607,17.875,19.28,28.92 C216.002,66.402,218.354,78.199,218.354,90.682z M128.164,135.222c6.078,0,11.895-1.144,17.384-3.463 c5.49-2.288,10.294-5.458,14.346-9.51c4.052-4.052,7.222-8.823,9.51-14.28c2.319-5.425,3.464-11.208,3.464-17.287 c0-6.078-1.145-11.895-3.464-17.385c-2.288-5.49-5.458-10.326-9.51-14.509c-4.052-4.15-8.791-7.385-14.247-9.672 c-5.457-2.288-11.274-3.464-17.483-3.464c-6.274,0-12.091,1.177-17.45,3.464c-5.359,2.287-10.098,5.522-14.248,9.672 c-4.183,4.183-7.385,8.986-9.706,14.444c-2.287,5.424-3.431,11.241-3.431,17.45c0,6.079,1.144,11.862,3.431,17.287 c2.32,5.457,5.523,10.228,9.706,14.28c4.15,4.052,8.888,7.222,14.248,9.51C116.072,134.078,121.889,135.222,128.164,135.222z" class="shape" style="-webkit-transform: scale(0.1015625, 0.08984375);-moz-transform: scale(0.1015625, 0.08984375);transform: scale(0.1015625, 0.08984375);"></path></svg></body></html>

</div><script>(function() {Preview.mElements.set(1059797, document.getElementById('el1059797_58f7675a995005ba58c0dd123efbdb21'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input name="location.state"  type="text" class=" " style="left:365px;top:331px;z-index:10049;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:237px;height:30px;position:absolute;" id="el225796_5b9b2875f0b00a3feedc16a1727df888"  value="">
    </div>
    <script>(function() {Preview.mElements.set(225796, document.getElementById('el225796_5b9b2875f0b00a3feedc16a1727df888'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script><div id="el891639_d33eb2a71cbe1cad12f5c85f1a6f70a0" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10053;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:335px;top:380px;font-family:;border-width:0px;border-style:solid;width:22px;height:27px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><path fill='rgba(135,76,16,0.6)' fill="#343434" d="M256,114.163c0,4.874-1.53,9.18-4.534,12.921c-3.003,3.684-6.772,6.063-11.334,7.197v51.228 c0,5.837-2.097,10.88-6.29,15.018c-4.222,4.193-9.237,6.232-15.073,6.232c-6.291-6.289-13.884-12.354-22.781-18.304 c-8.896-5.949-18.474-11.39-28.674-16.32c-10.258-4.93-20.798-9.18-31.621-12.693c-10.824-3.514-21.336-5.837-31.508-6.97 c-3.995,1.133-7.253,2.945-9.747,5.553c-2.493,2.606-4.193,5.497-5.129,8.671c-0.935,3.23-1.02,6.459-0.283,9.803 c0.766,3.344,2.466,6.348,5.129,9.011c-2.295,3.796-3.372,7.31-3.174,10.54c0.17,3.174,1.134,6.234,2.834,9.236 c1.7,2.947,4.023,5.781,6.886,8.501c2.86,2.721,5.921,5.44,9.151,8.16c-1.842,3.967-5.044,6.971-9.634,9.067 s-9.492,3.287-14.733,3.57c-5.214,0.283-10.343-0.227-15.357-1.474c-4.986-1.247-8.925-3.287-11.787-6.063 c-1.672-5.61-3.514-11.391-5.554-17.341c-2.039-5.949-3.711-11.957-4.986-18.134c-1.304-6.177-1.983-12.523-1.983-19.154 c0-6.63,1.134-13.657,3.344-21.08H21.392c-5.836,0-10.88-2.04-15.073-6.233C2.096,140.967,0,135.924,0,129.974V98.183 c0-5.837,2.067-10.881,6.262-15.073c4.137-4.251,9.181-6.348,15.13-6.348h69.306c10.654,0,21.987-1.53,34.029-4.646 c12.042-3.117,23.829-7.253,35.418-12.467c11.588-5.271,22.47-11.164,32.726-17.794c10.201-6.631,18.871-13.431,25.898-20.515 c5.836,0,10.852,2.097,15.073,6.291c4.193,4.137,6.29,9.18,6.29,15.13v51.115c4.562,1.077,8.331,3.514,11.334,7.254 C254.47,104.926,256,109.233,256,114.163z M218.769,49.562c-7.027,5.439-14.733,10.71-23.064,15.754 c-8.33,5.043-17.171,9.633-26.464,13.657c-9.294,4.08-18.813,7.706-28.477,10.767c-9.661,3.116-19.239,5.384-28.674,6.743v35.305 c9.435,1.474,19.013,3.74,28.674,6.8c9.663,3.061,19.183,6.688,28.477,10.824c9.293,4.193,18.189,8.783,26.605,13.771 s16.065,10.199,22.923,15.526V49.562z" class="shape" style="-webkit-transform: scale(0.0859375, 0.10546875);-moz-transform: scale(0.0859375, 0.10546875);transform: scale(0.0859375, 0.10546875);"></path></svg></body></html>

</div><script>(function() {Preview.mElements.set(891639, document.getElementById('el891639_d33eb2a71cbe1cad12f5c85f1a6f70a0'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input name="event.name"  type="text" class=" " style="z-index:10050;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-size:auto;background-repeat:repeat;background-position:center center;background-image:url('');box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;left:365px;top:378px;width:237px;height:30px;position:absolute;" id="el913519_936523f0f478dde7e81d9afb3f3ec40e"  value="">
    </div>
    <script>(function() {Preview.mElements.set(913519, document.getElementById('el913519_936523f0f478dde7e81d9afb3f3ec40e'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <input type="submit" onclick="submitPostForm('postForm');" style=";left:29px;top:440px;z-index:10046;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;" class="ElButton active " value="Post" id="el590169_fae9630e607979c77a4cced91bc83251" />
    <script>(function() {Preview.mElements.set(590169, document.getElementById('el590169_fae9630e607979c77a4cced91bc83251'), {"interactive":true,"in_group":true,"in_component":false,"id_component":false})})();</script>    <input type="button" style=";z-index:10047;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;left:477px;top:440px;width:120px;height:30px;position:absolute;;" class="ElButton active " value="Cancel" id="el204524_7f8f905dca3b5acf75c9f24d52af331e" />
    <script>(function() {Preview.mElements.set(204524, document.getElementById('el204524_7f8f905dca3b5acf75c9f24d52af331e'), {"interactive":true,"in_group":true,"in_component":false,"id_component":false})})();</script><div id="el1511728_a40a188c52efa4283500ac051b904574" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10054;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:161px;top:445px;font-family:;border-width:0px;border-style:solid;width:19px;height:19px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><path fill='rgba(135,76,16,0.6)' fill="#343434" d="M44.667,74.854c3.361,0,5.972,0.861,7.833,2.639s3.222,3.889,4,6.333c0.833,2.444,1.333,5.027,1.5,7.694 c0.194,2.667,0.278,4.833,0.278,6.472v20.695c-1.278,0.722-2.333,1.555-3.195,2.5c-0.861,0.972-2.111,1.444-3.75,1.444H6.667 c-1.445,0-2.639-0.473-3.611-1.444c-0.944-0.945-1.944-1.778-3.056-2.5V97.993c0-1.639,0.056-3.805,0.139-6.472 c0.083-2.667,0.556-5.25,1.361-7.694c0.833-2.444,2.167-4.556,4.028-6.333s4.472-2.639,7.805-2.639 c-4.055-2.639-7.305-6.083-9.722-10.305C1.222,60.326,0,55.576,0,50.326c0-3.917,0.778-7.639,2.25-11.167 c1.5-3.527,3.583-6.639,6.25-9.333c2.694-2.667,5.806-4.75,9.333-6.25c3.556-1.5,7.278-2.25,11.167-2.25 c4.111,0,7.917,0.75,11.444,2.25c3.556,1.5,6.611,3.583,9.194,6.25c2.583,2.694,4.694,5.806,6.25,9.333 c1.611,3.528,2.389,7.25,2.389,11.167c0,5.167-1.222,9.917-3.722,14.167S48.778,72.215,44.667,74.854z M215.277,140.465 c6,5.361,10.723,10.667,14.111,15.889c3.389,5.209,5.111,11.153,5.111,17.875v52.723c-1.111,0.5-2,1.111-2.723,1.777 c-0.722,0.611-1.555,1.223-2.527,1.889c-0.973,0.611-2.027,1.278-3.194,1.945c-1.167,0.722-2.833,1.389-4.889,2.111H34.861 c-3.167,0-5.639-1-7.417-3.057c-1.777-2.055-3.722-3.61-5.917-4.666v-52.723c0-7.055,2.028-13.333,6.139-18.847 c4.055-5.473,8.5-10.473,13.333-14.917c0.889-0.889,2.056-1.889,3.472-2.916c1.417-1.057,2.917-1.75,4.556-2.111 c1.639-0.556,3.5-0.861,5.583-0.973c2.083-0.083,4.111-0.306,6.111-0.666c5.556-0.89,11.445-1.778,17.722-2.667 c6.278-0.833,12.389-1.723,18.389-2.639c-8.166-5.194-14.722-12.055-19.666-20.639c-4.973-8.555-7.445-18-7.445-28.25 c0-7.972,1.556-15.527,4.639-22.666c3.083-7.139,7.25-13.306,12.527-18.528c5.278-5.222,11.444-9.361,18.5-12.444 C112.5,22.854,120,21.326,128,21.326s15.527,1.527,22.611,4.639c7.083,3.083,13.25,7.223,18.527,12.444 c5.25,5.223,9.417,11.389,12.528,18.528c3.083,7.139,4.61,14.694,4.61,22.666c0,10.25-2.444,19.639-7.333,28.195 c-4.917,8.528-11.5,15.444-19.75,20.694c5.973,0.917,12.083,1.75,18.306,2.583c6.223,0.833,12.167,1.723,17.777,2.723 c2.084,0.36,4.111,0.583,6.111,0.666c2,0.111,3.889,0.417,5.611,0.973c1.611,0.361,3.139,1.055,4.556,2.111 C212.944,138.576,214.223,139.576,215.277,140.465z M242.667,74.854c3.36,0,5.916,0.861,7.694,2.639 c1.75,1.778,3.056,3.889,3.861,6.333c0.833,2.444,1.333,5.027,1.5,7.694c0.194,2.667,0.277,4.833,0.277,6.472v20.695 c-1.083,0.722-2.111,1.555-3.056,2.5c-0.944,0.972-2.167,1.444-3.611,1.444h-44.666c-1.611,0-2.89-0.473-3.723-1.444 c-0.889-0.945-1.944-1.778-3.222-2.5V97.993c0-1.639,0.11-3.805,0.277-6.472s0.723-5.25,1.556-7.694 c0.889-2.444,2.222-4.556,4.111-6.333c1.833-1.778,4.416-2.639,7.666-2.639c-4.056-2.639-7.36-6.083-9.86-10.305 s-3.75-8.972-3.75-14.222c0-3.917,0.75-7.639,2.25-11.167c1.5-3.527,3.583-6.639,6.25-9.333c2.694-2.667,5.805-4.75,9.333-6.25 c3.556-1.5,7.361-2.25,11.444-2.25c3.889,0,7.611,0.75,11.167,2.25s6.666,3.583,9.333,6.25c2.667,2.694,4.777,5.806,6.277,9.333 c1.5,3.528,2.223,7.25,2.223,11.167c0,5.167-1.194,9.917-3.611,14.167C250,68.743,246.75,72.215,242.667,74.854z" class="shape" style="-webkit-transform: scale(0.07421875, 0.07421875);-moz-transform: scale(0.07421875, 0.07421875);transform: scale(0.07421875, 0.07421875);"></path></svg></body></html>

</div><script>(function() {Preview.mElements.set(1511728, document.getElementById('el1511728_a40a188c52efa4283500ac051b904574'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>  
<div class="modal-overlay" style="display: none;z-index: 10042"></div>
</form>
<div class="ElDialog " style="z-index:10042;border-width:1px;border-radius:0px;border-color:#c7c7c7;border-style:solid;background-color:#F7F5F5;left:0px;top:0px;font-family:;width:623px;height:493px;position:absolute" id="el553773_fb6cf309309a8b1e4b78ed45b79ce23b" >
  <div class="topbar" style="color:#e6b907;font-weight:bold;font-style:normal;font-size:14px;font-family:Trebuchet MS;text-align:none;background-color:#333025;display:block;height: 24px;line-height: 24px">
    <div class="text">Tell us about what you&#x27;ve seen</div>
  </div>
  <div class="close">&times;</div>
  <div class="content" style="color:#666;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;background-color:rgba(250,229,0,0.3)">
    <div class="text" style="padding-top: 54px"></div>
  </div>
  <div class="bottombar" style="background-color:#ffffff;display:none"></div>
</div>
<script>(function() {Preview.mElements.set(553773, document.getElementById('el553773_fb6cf309309a8b1e4b78ed45b79ce23b'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    </div>
    <script>(function() {Preview.mElements.set(1203718, document.getElementById('el1203718_4b6d07e448d9a9b4f63597e4756327aa'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElGroup " style="z-index:10076;left:164px;top:377px;font-family:;border-width:0px;border-style:solid;visibility:hidden;width:503px;height:629px;position:absolute;" id="el2689083_101fb7b4d1fe58269c1b97fa47a8758d" >
          <div class="ElTextElement " style="left:21px;top:51px;z-index:10060;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:75px;height:24px;position:absolute;" id="el5900875_eb774442a5944a5701122d15fd24c887" ><p class="wysiwyg-font-type-Arvo"><span style="font-weight: bold;">Account</span></p></div>
    <script>(function() {Preview.mElements.set(5900875, document.getElementById('el5900875_eb774442a5944a5701122d15fd24c887'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>  
<div class="modal-overlay" style="display: none;z-index: 10057"></div>

<div class="ElDialog " style="left:0px;top:0px;z-index:10057;border-width:1px;border-radius:0px;border-color:#c7c7c7;border-style:solid;background-color:#F7F5F5;font-family:;width:501px;height:627px;position:absolute" id="el1086239_35eb52665b2c6877056167bb33a43de4" >
  <div class="topbar" style="color:#e6b907;font-weight:bold;font-style:normal;font-size:14px;font-family:Trebuchet MS;text-align:none;background-color:#333025;display:block;height: 24px;line-height: 24px">
    <div class="text">Change your personal settings here</div>
  </div>
  <div class="close">&times;</div>
  <div class="content" style="color:#666;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;background-color:rgba(250,229,0,0.3)">
    <div class="text" style="padding-top: 54px"></div>
  </div>
  <div class="bottombar" style="background-color:#ffffff;display:none"></div>
</div>
<script>(function() {Preview.mElements.set(1086239, document.getElementById('el1086239_35eb52665b2c6877056167bb33a43de4'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="text" class=" " style="left:141px;top:91px;z-index:10062;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el7047278_00502926a2bdc45722754f3e4ea975d2"  value="">
    </div>
    <script>(function() {Preview.mElements.set(7047278, document.getElementById('el7047278_00502926a2bdc45722754f3e4ea975d2'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:36px;top:96px;z-index:10063;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:99px;height:23px;position:absolute;" id="el8388672_7478c7add0dde50a0d8ac49a625ac0de" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">Email</p></div>
    <script>(function() {Preview.mElements.set(8388672, document.getElementById('el8388672_7478c7add0dde50a0d8ac49a625ac0de'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:36px;top:157px;z-index:10065;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:105px;height:23px;position:absolute;" id="el6069034_ec30e26b551480b07ee7799ede0597a8" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">First Name</p></div>
    <script>(function() {Preview.mElements.set(6069034, document.getElementById('el6069034_ec30e26b551480b07ee7799ede0597a8'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="text" class=" " style="left:141px;top:151px;z-index:10064;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el5639657_a3d0f5c43a4faa647a90f6b48483cf01"  value="">
    </div>
    <script>(function() {Preview.mElements.set(5639657, document.getElementById('el5639657_a3d0f5c43a4faa647a90f6b48483cf01'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:36px;top:212px;z-index:10067;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:105px;height:23px;position:absolute;" id="el8246124_dcfa71b0d8690b6317f743ed0c5364e0" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">Last Name</p></div>
    <script>(function() {Preview.mElements.set(8246124, document.getElementById('el8246124_dcfa71b0d8690b6317f743ed0c5364e0'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="text" class=" " style="left:141px;top:206px;z-index:10066;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el3073017_d99001d0fa294c526e3c1c41453607fb"  value="">
    </div>
    <script>(function() {Preview.mElements.set(3073017, document.getElementById('el3073017_d99001d0fa294c526e3c1c41453607fb'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:36px;top:270px;z-index:10068;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:23px;position:absolute;" id="el103019_81a8af13ba41a41dd03cd69187db635e" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">Country</p></div>
    <script>(function() {Preview.mElements.set(103019, document.getElementById('el103019_81a8af13ba41a41dd03cd69187db635e'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <select class="" style="left:143px;top:265px;multiple:disabled;z-index:10075;background-color:#ffffff;border-width:1px;border-radius:0px;border-color:#c8c8c8;border-style:solid;color:#666;font-size:14px;font-family:Arial;text-align:left;width:303px;height:31px;position:absolute;" id="el2673762_45971db8b55e6d38334a227421da18c6" ><option value="USA">USA</option><option value="Philippines">Philippines</option><option value="Indonesia">Indonesia</option></select><script>(function() {Preview.mElements.set(2673762, document.getElementById('el2673762_45971db8b55e6d38334a227421da18c6'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:21px;top:324px;z-index:10061;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:145px;height:24px;position:absolute;" id="el803660_09124abcbb618a3677cbaf14bf45022e" ><p class="wysiwyg-font-type-Arvo"><span style="font-weight: bold;">Change Password</span></p></div>
    <script>(function() {Preview.mElements.set(803660, document.getElementById('el803660_09124abcbb618a3677cbaf14bf45022e'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:41px;top:376px;z-index:10072;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:41px;position:absolute;" id="el7899589_09281b9c8b189c24fbf4898ec1f9865d" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">Current Password</p></div>
    <script>(function() {Preview.mElements.set(7899589, document.getElementById('el7899589_09281b9c8b189c24fbf4898ec1f9865d'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="password" class=" " style="left:141px;top:376px;z-index:10071;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el5333821_bb0b1e372788db9d348326785a870c58"  value="">
    </div>
    <script>(function() {Preview.mElements.set(5333821, document.getElementById('el5333821_bb0b1e372788db9d348326785a870c58'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:41px;top:445px;z-index:10070;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:23px;position:absolute;" id="el4580197_4f03f89ce4fe2ac93fda94b2a514809e" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">New Password</p></div>
    <script>(function() {Preview.mElements.set(4580197, document.getElementById('el4580197_4f03f89ce4fe2ac93fda94b2a514809e'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="password" class=" " style="left:141px;top:439px;z-index:10069;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el5942999_4d4ed0e08d40541d0f1e34a64558d13d"  value="">
    </div>
    <script>(function() {Preview.mElements.set(5942999, document.getElementById('el5942999_4d4ed0e08d40541d0f1e34a64558d13d'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="left:41px;top:499px;z-index:10074;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:41px;position:absolute;" id="el2375065_b67caf142ff8eac18e45c75a27c94764" ><p class="wysiwyg-font-type-Cabin" style="font-size: 14px;">Confirm Password</p></div>
    <script>(function() {Preview.mElements.set(2375065, document.getElementById('el2375065_b67caf142ff8eac18e45c75a27c94764'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <div class="ElInputWithoutLabel">
      <input   type="password" class=" " style="left:141px;top:499px;z-index:10073;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;" id="el5708752_b39727acdd656e102f5f9bd10593b481"  value="">
    </div>
    <script>(function() {Preview.mElements.set(5708752, document.getElementById('el5708752_b39727acdd656e102f5f9bd10593b481'), {"interactive":false,"in_group":true,"in_component":false,"id_component":false})})();</script>    <input type="button" style=";left:41px;top:570px;z-index:10058;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;" class="ElButton active " value="Save" id="el6017404_96778fe6e18530358a5bcf21074f0630" />
    <script>(function() {Preview.mElements.set(6017404, document.getElementById('el6017404_96778fe6e18530358a5bcf21074f0630'), {"interactive":true,"in_group":true,"in_component":false,"id_component":false})})();</script>    <input type="button" style=";left:338px;top:570px;z-index:10059;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;" class="ElButton active " value="Cancel" id="el7997034_5dd9d3782aeffb241eede16efeb1f83c" />
    <script>(function() {Preview.mElements.set(7997034, document.getElementById('el7997034_5dd9d3782aeffb241eede16efeb1f83c'), {"interactive":true,"in_group":true,"in_component":false,"id_component":false})})();</script>    </div>
    <script>(function() {Preview.mElements.set(2689083, document.getElementById('el2689083_101fb7b4d1fe58269c1b97fa47a8758d'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
<script>
Interactions.ori_styles = {"2646256":{"opacity":null,"width":995,"height":345,"top":0,"left":0},"2223556":{"opacity":null,"width":260,"height":245,"top":63,"left":597},"351375":{"opacity":null,"width":300,"height":109,"top":118,"left":80},"1986503":{"opacity":null,"width":279,"height":265,"top":53,"left":588},"599320":{"opacity":null,"width":300,"height":24,"top":223,"left":80},"3552193":{"opacity":null,"width":52,"height":52,"top":247,"left":892},"2323527":{"opacity":null,"width":52,"height":52,"top":67,"left":892},"4075710":{"opacity":null,"width":52,"height":52,"top":159,"left":892},"3902599":{"opacity":null,"width":32,"height":32,"top":256,"left":902,"original_width":"256 ","original_height":"256 \n"},"494362":{"opacity":null,"width":32,"height":32,"top":169,"left":902,"original_width":"26 ","original_height":"26 \n"},"3781894":{"opacity":null,"width":32,"height":32,"top":77,"left":902,"original_width":"32 ","original_height":"32 \n"},"149217":{"opacity":null,"width":118,"height":24,"top":353,"left":867},"3527237":{"opacity":null,"width":117,"height":24,"top":353,"left":15},"2437029":{"opacity":null,"width":125,"height":24,"top":353,"left":155},"3255003":{"opacity":null,"width":77,"height":24,"top":353,"left":313},"1361103":{"opacity":null,"width":115,"height":24,"top":353,"left":415},"2827483":{"opacity":null,"width":108,"height":24,"top":353,"left":740},"1684143":{"opacity":null,"width":995,"height":40,"top":345,"left":0},"2460782":{"opacity":null,"width":17,"height":656,"top":385,"left":705},"762864":{"opacity":null,"width":17,"height":17,"top":639,"left":0},"2585818":{"opacity":null,"width":5,"height":640,"top":0,"left":6},"498096":{"opacity":null,"width":400,"height":975,"top":400,"left":14},"2673401":{"opacity":null,"width":150,"height":24,"top":830,"left":735},"2918070":{"opacity":null,"width":150,"height":24,"top":400,"left":735},"5389848":{"opacity":null,"width":150,"height":24,"top":616,"left":735},"7265272":{"opacity":null,"width":273,"height":975,"top":400,"left":425},"579276":{"opacity":null,"width":213,"height":21,"top":985,"left":761},"5898848":{"opacity":null,"width":213,"height":21,"top":860,"left":761},"5683157":{"opacity":null,"width":213,"height":21,"top":890,"left":761},"41172":{"opacity":null,"width":213,"height":21,"top":920,"left":761},"1940600":{"opacity":null,"width":213,"height":21,"top":950,"left":761},"2045763":{"opacity":null,"width":213,"height":21,"top":525,"left":761},"2297268":{"opacity":null,"width":213,"height":21,"top":465,"left":761},"3827748":{"opacity":null,"width":213,"height":21,"top":435,"left":761},"6624788":{"opacity":null,"width":213,"height":21,"top":495,"left":761},"7472692":{"opacity":null,"width":213,"height":21,"top":560,"left":761},"57203":{"opacity":null,"width":213,"height":21,"top":771,"left":761},"1275685":{"opacity":null,"width":213,"height":21,"top":676,"left":761},"1322238":{"opacity":null,"width":213,"height":21,"top":736,"left":761},"4702536":{"opacity":null,"width":213,"height":21,"top":706,"left":761},"6249759":{"opacity":null,"width":213,"height":21,"top":646,"left":761},"1203718":{"opacity":null,"width":625,"height":495,"top":430,"left":125},"454048":{"opacity":null,"width":300,"height":353,"top":56,"left":20},"810973":{"opacity":null,"width":50,"height":21,"top":56,"left":335},"928834":{"opacity":null,"width":267,"height":187,"top":80,"left":335},"743961":{"opacity":null,"width":18,"height":21,"top":289,"left":335,"original_width":"256 ","original_height":"256 \n"},"672685":{"opacity":null,"width":237,"height":30,"top":284,"left":365},"1059797":{"opacity":null,"width":26,"height":23,"top":335,"left":331,"original_width":"256 ","original_height":"256 \n"},"225796":{"opacity":null,"width":237,"height":30,"top":331,"left":365},"891639":{"opacity":null,"width":22,"height":27,"top":380,"left":335,"original_width":"256 ","original_height":"256 \n"},"913519":{"opacity":null,"width":237,"height":30,"top":378,"left":365},"590169":{"opacity":null,"width":120,"height":30,"top":440,"left":29},"204524":{"opacity":null,"width":120,"height":30,"top":440,"left":477},"1511728":{"opacity":null,"width":19,"height":19,"top":445,"left":161,"original_width":"256 ","original_height":"256 \n"},"553773":{"opacity":null,"width":625,"height":495,"top":0,"left":0},"2689083":{"opacity":null,"width":503,"height":629,"top":377,"left":164},"5900875":{"opacity":null,"width":75,"height":24,"top":51,"left":21},"1086239":{"opacity":null,"width":503,"height":629,"top":0,"left":0},"7047278":{"opacity":null,"width":300,"height":30,"top":91,"left":141},"8388672":{"opacity":null,"width":99,"height":23,"top":96,"left":36},"6069034":{"opacity":null,"width":105,"height":23,"top":157,"left":36},"5639657":{"opacity":null,"width":300,"height":30,"top":151,"left":141},"8246124":{"opacity":null,"width":105,"height":23,"top":212,"left":36},"3073017":{"opacity":null,"width":300,"height":30,"top":206,"left":141},"103019":{"opacity":null,"width":100,"height":23,"top":270,"left":36},"2673762":{"opacity":null,"width":305,"height":33,"top":265,"left":143},"803660":{"opacity":null,"width":145,"height":24,"top":324,"left":21},"7899589":{"opacity":null,"width":100,"height":41,"top":376,"left":41},"5333821":{"opacity":null,"width":300,"height":30,"top":376,"left":141},"4580197":{"opacity":null,"width":100,"height":23,"top":445,"left":41},"5942999":{"opacity":null,"width":300,"height":30,"top":439,"left":141},"2375065":{"opacity":null,"width":100,"height":41,"top":499,"left":41},"5708752":{"opacity":null,"width":300,"height":30,"top":499,"left":141},"6017404":{"opacity":null,"width":120,"height":30,"top":570,"left":41},"7997034":{"opacity":null,"width":120,"height":30,"top":570,"left":338}};
Interactions.scrollable_el = document.getElementById('canvas-wrapper');

      document.getElementById('el3902599_f91801ebe8b82889fe31f13cbe60b175') && document.getElementById('el3902599_f91801ebe8b82889fe31f13cbe60b175').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '1203718';
  var el = Preview.mElements.get(id);
  var animation = {"show":"slide","time":"300","style":"ease-in"};
  var is_scroll = false;
  var is_keydown = false;  
  var event_value = null;

  if (
    (is_scroll && 0 === Interactions.ifFireOneTimeScroll(id, 'visibility', event_value)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  if (el.isVisible()) {
    el.hide(animation);
  }
  else {
    el.show(animation);
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394') && document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 494362;
  var animation = null;
  var styles = {"color":"#f5ce0a"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394') && document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 494362;
  var animation = null;
  var styles = {"color":"#000000"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394') && document.getElementById('el494362_ce373ea5d85d5cd67cccf8953fb89394').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '2689083';
  var el = Preview.mElements.get(id);
  var animation = {"show":"slide","time":"300","style":"ease-in"};
  var is_scroll = false;
  var is_keydown = false;  
  var event_value = null;

  if (
    (is_scroll && 0 === Interactions.ifFireOneTimeScroll(id, 'visibility', event_value)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  if (el.isVisible()) {
    el.hide(animation);
  }
  else {
    el.show(animation);
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el3781894_e86f857320b912c425f0d2d11b9ea4de') && document.getElementById('el3781894_e86f857320b912c425f0d2d11b9ea4de').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 3781894;
  var animation = null;
  var styles = {"color":"#29cc04"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970') && document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 3527237;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":"#ffffff"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970') && document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 3527237;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":[]};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970') && document.getElementById('el3527237_4ba106ad60a51621ef06489cce1e4970').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function() {
  var action = {"target":false,"url":"\/groups"};

  if (action.target) {
    window.open(action.url, action.target);
  }
  else {
    window.location.href = action.url;
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684') && document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 2437029;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":"#ffffff"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684') && document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 2437029;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":[]};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684') && document.getElementById('el2437029_ff953cb804f1b5813828f0f08268d684').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function() {
  var action = {"target":false,"url":"\/ztookerfeeds"};

  if (action.target) {
    window.open(action.url, action.target);
  }
  else {
    window.location.href = action.url;
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366') && document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 3255003;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":"#ffffff"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366') && document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 3255003;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":[]};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366') && document.getElementById('el3255003_00b5c69262da1441fb959009bd42a366').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function() {
  var action = {"target":false,"url":"\/postfeeds"};

  if (action.target) {
    window.open(action.url, action.target);
  }
  else {
    window.location.href = action.url;
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02') && document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 1361103;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":"#ffffff"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02') && document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 1361103;
  var animation = {"style":"linear","time":"300","show":""};
  var styles = {"color":[]};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02') && document.getElementById('el1361103_a1a799bf85dccc7ca1355bcd7243cc02').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function() {
  var action = {"target":false,"url":"\/spottedfeeds"};

  if (action.target) {
    window.open(action.url, action.target);
  }
  else {
    window.location.href = action.url;
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b') && document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b').addEventListener('mouseenter', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 2827483;
  var animation = {"time":"300","show":"","style":"linear"};
  var styles = {"color":"#ffffff"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b') && document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b').addEventListener('mouseleave', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = 2827483;
  var animation = {"time":"300","show":"","style":"linear"};
  var styles = {"color":[]};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = true;
  var event_value = null;

  if (is_scroll) {
    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);

    if (fire_factor === 0) {
      return false;
    }

    if (fire_factor === -1) {
      var ori_styles = Interactions.ori_styles[id];

      for (var style in styles) {
        if (styles.hasOwnProperty(style)) {
          styles[style] = ori_styles[style];
        }
      }
    }
  }
  else if (is_keydown && e.keyCode !== parseInt(event_value)) {
    return false;
  }

  // set one animation for all changed styles
  Preview.mElements.get(id).setAnimation('style', animation);

  for (var style in styles) {
    if (styles.hasOwnProperty(style)) {
      var style_value = styles[style];
      var update_original = true;

      if (is_reverse && Interactions.ori_styles[id][style]) {
        style_value = Interactions.ori_styles[id][style];
        update_original = false;
      }

      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);
    }
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
      document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b') && document.getElementById('el2827483_a5515c27331ed39e873ae54a81befe9b').addEventListener('click', function(e) {
      var processed = [];

       processed.push((function() {
      var action = {"target":false,"url":"..\/stookList\/${loggedUser.id}"};

  if (action.target) {
    window.open(action.url, action.target);
  }
  else {
    window.location.href = action.url;
  }
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el590169_fae9630e607979c77a4cced91bc83251') && document.getElementById('el590169_fae9630e607979c77a4cced91bc83251').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '1203718';
  var el = Preview.mElements.get(id);
  var animation = {"show":"slide","time":"300","style":"ease-out"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (
    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  el.hide(animation);
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el204524_7f8f905dca3b5acf75c9f24d52af331e') && document.getElementById('el204524_7f8f905dca3b5acf75c9f24d52af331e').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '1203718';
  var el = Preview.mElements.get(id);
  var animation = {"show":"fade","time":"300","style":"ease-out"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (
    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  el.hide(animation);
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el6017404_96778fe6e18530358a5bcf21074f0630') && document.getElementById('el6017404_96778fe6e18530358a5bcf21074f0630').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '2689083';
  var el = Preview.mElements.get(id);
  var animation = {"show":"slide","time":"300","style":"ease-out"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (
    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  el.hide(animation);
})(e));
              

      Interactions.handleEvent(e, processed);
    });
        document.getElementById('el7997034_5dd9d3782aeffb241eede16efeb1f83c') && document.getElementById('el7997034_5dd9d3782aeffb241eede16efeb1f83c').addEventListener('click', function(e) {
      var processed = [];

                        processed.push((function(e) {
  var id = '2689083';
  var el = Preview.mElements.get(id);
  var animation = {"show":"fade","time":"300","style":"linear"};
  var is_scroll = false;
  var is_keydown = false;
  var is_reverse = false;
  var event_value = null;

  if (
    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||
    (is_keydown && e.keyCode !== parseInt(event_value, 10))
  ) {
    return false;
  }

  el.hide(animation);
})(e));
              

      Interactions.handleEvent(e, processed);
    });
  
</script>

            </div>

            <div id="comments-pins"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
var uri = new URI(document.URL);
Preview.attachEvents();

window.addEvent('domready', function() {
    var data = {"id_project":"1170098","id_collection":"237370","id_page":"11813072","id_document":null,"version_of":"11813071","project_hash":"1ef043e343fde09296497ec728793809362292cc","active_collaboration":false,"menu_enabled":false,"export_mode":true,"add_comments":false,"page_base_url":null,"pages":[],"comments":null,"is_user_logged_in":false,"versions":"{\"5\":\"11813072\"}","current_version":"5","current_map":[],"versions_html":{"11813072":"    <div class=\"ElBox \" style=\"z-index:10001;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:transparent;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:data\/113147_120237_escheresque_ste.png;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:0px;top:0px;width:995px;height:345px;position:absolute;background-image: url(data\/113147_120237_escheresque_ste.png);\" id=\"el2646256_c515b23d153fc74f724eb4322e474daa\" ><\/div>\n    <script>(function() {Preview.mElements.set(2646256, document.getElementById('el2646256_c515b23d153fc74f724eb4322e474daa'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>\n      <div class=\"ElImageWrapper \" id=\"imgAvatar\" style=\"z-index:10004;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:597px;top:63px;width:260px;height:245px;position:absolute;\" >\n                      <img class=\"ElImage \" src=\"..\/data\/${user.id}.jpg\" \/>\n                <\/div>\n\n    <script>(function() {Preview.mElements.set(2223556, document.getElementById('imgAvatar'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:80px;top:118px;z-index:10002;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:109px;position:absolute;\" id=\"el351375_9ceed30a6abba5fedd7f8821dcab3a1d\" ><p class=\"wysiwyg-font-type-Arvo\" style=\"font-size: 84px;\"><span style=\"font-weight: bold; background-color: rgba(186, 140, 132, 0); color: rgb(230, 185, 7);\">Ztook<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(351375, document.getElementById('el351375_9ceed30a6abba5fedd7f8821dcab3a1d'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElBox \" style=\"z-index:10003;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#5c4221;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:data\/113147_120237_congruent_pentagon_brown.png;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:588px;top:53px;width:279px;height:265px;position:absolute;background-image: url(data\/113147_120237_congruent_pentagon_brown.png);\" id=\"el1986503_1129c6675a68aefa36a8e13feb3f707d\" ><\/div>\n    <script>(function() {Preview.mElements.set(1986503, document.getElementById('el1986503_1129c6675a68aefa36a8e13feb3f707d'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10006;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:80px;top:223px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:24px;position:absolute;\" id=\"el599320_f90ad8f35b9cf2e420a992cbe767c866\" ><p class=\"wysiwyg-font-type-Arvo\"><span style=\"color: rgb(230, 185, 7);\">Welcome, ${user.firstName} ${user.lastName}!<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(599320, document.getElementById('el599320_f90ad8f35b9cf2e420a992cbe767c866'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el3552193_66417df670268aa8537191c5aef018f6\" class=\"GeoSVGEl \" style=\"z-index:10009;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;left:892px;top:247px;font-family:;border-width:0px;border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;\">\n <svg version=\"1.2\" width=\"100%\" height=\"100%\">\n  <ellipse class=\"shape\" cx=\"50\" cy=\"50\" rx=\"50\" ry=\"50\" fill=\"rgba(194,165,112,0.5)\" style=\"-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.52,0.52); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;\" \/>\n <\/svg>\n<\/div><script>(function() {Preview.mElements.set(3552193, document.getElementById('el3552193_66417df670268aa8537191c5aef018f6'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el2323527_99628e490a001b48f9f5224c1076364e\" class=\"GeoSVGEl \" style=\"left:892px;top:67px;z-index:10007;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;font-family:;border-width:0px;border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;\">\n <svg version=\"1.2\" width=\"100%\" height=\"100%\">\n  <ellipse class=\"shape\" cx=\"50\" cy=\"50\" rx=\"50\" ry=\"50\" fill=\"rgba(194,165,112,0.5)\" style=\"-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.52,0.52); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;\" \/>\n <\/svg>\n<\/div><script>(function() {Preview.mElements.set(2323527, document.getElementById('el2323527_99628e490a001b48f9f5224c1076364e'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el4075710_1b1333bed1944a75e4449dc6306e27e2\" class=\"GeoSVGEl \" style=\"left:892px;top:159px;z-index:10008;color:rgba(194,165,112,0.5);stroke:3;element:ellipse;font-family:;border-width:0px;border-style:solid;width:52px;height:52px;position:absolute;-webkit-transform-origin: 26px 26px;-moz-transform-origin: 26px 26px;-ms-transform-origin: 26px 26px;-o-transform-origin: 26px 26px;\">\n <svg version=\"1.2\" width=\"100%\" height=\"100%\">\n  <ellipse class=\"shape\" cx=\"50\" cy=\"50\" rx=\"50\" ry=\"50\" fill=\"rgba(194,165,112,0.5)\" style=\"-webkit-transform: scale(0.52,0.52); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.52,0.52); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.52,0.52); -ms-transform-origin: 0% 0%; -o-transform: scale(0.52,0.52); -o-transform-origin: 0% 0%;\" \/>\n <\/svg>\n<\/div><script>(function() {Preview.mElements.set(4075710, document.getElementById('el4075710_1b1333bed1944a75e4449dc6306e27e2'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"btnSpotPost\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10010;color:#000000;rotation:0;current_element_category_id:20973;left:902px;top:256px;current_element_id:1592;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><path fill='#000000' fill=\"#343434\" d=\"M240.16,53.151c4.364,0,8.09,1.615,11.193,4.846c3.103,3.258,4.646,7.056,4.646,11.391v149.33 c0,4.336-1.544,8.104-4.646,11.25c-3.104,3.145-6.829,4.703-11.193,4.703H15.968c-4.35,0-8.104-1.559-11.25-4.703 C1.573,226.82,0,223.053,0,218.717V69.387c0-4.335,1.573-8.132,4.718-11.391c3.145-3.23,6.9-4.846,11.25-4.846h50.849l8.047-17.398 c1.672-3.967,4.732-7.367,9.167-10.201c4.449-2.805,8.854-4.222,13.204-4.222h61.531c4.35,0,8.756,1.417,13.205,4.222 c4.435,2.833,7.537,6.234,9.294,10.201l7.92,17.398H240.16L240.16,53.151z M128.064,210.811c9.082,0,17.696-1.785,25.842-5.299 s15.217-8.301,21.182-14.365c5.979-6.064,10.725-13.148,14.238-21.195s5.285-16.719,5.285-25.955c0-9.096-1.771-17.739-5.285-25.928 c-3.514-8.188-8.26-15.272-14.238-21.251c-5.965-5.979-13.035-10.711-21.182-14.253c-8.146-3.514-16.761-5.271-25.842-5.271 c-9.067,0-17.709,1.757-25.899,5.271c-8.203,3.542-15.287,8.274-21.252,14.253c-5.979,5.979-10.725,13.035-14.238,21.195 c-3.528,8.133-5.285,16.803-5.285,25.985c0,9.236,1.757,17.908,5.285,25.955c3.514,8.047,8.26,15.131,14.238,21.195 c5.965,6.064,13.049,10.852,21.252,14.365C110.354,209.025,118.997,210.811,128.064,210.811z M128.064,98.687 c6.305,0,12.184,1.19,17.653,3.542c5.455,2.38,10.229,5.582,14.296,9.663c4.08,4.08,7.296,8.869,9.663,14.366 c2.352,5.526,3.541,11.418,3.541,17.739c0,6.29-1.189,12.156-3.541,17.625c-2.367,5.469-5.583,10.285-9.663,14.451 c-4.066,4.164-8.869,7.424-14.366,9.803c-5.512,2.354-11.377,3.514-17.583,3.514c-6.291,0-12.199-1.16-17.709-3.514 c-5.512-2.379-10.3-5.639-14.367-9.803c-4.081-4.166-7.296-9.012-9.663-14.508c-2.352-5.525-3.542-11.393-3.542-17.568 c0-6.32,1.19-12.213,3.542-17.739c2.366-5.497,5.582-10.286,9.663-14.366c4.066-4.081,8.855-7.282,14.367-9.663 C115.865,99.876,121.773,98.687,128.064,98.687z\" class=\"shape\" style=\"-webkit-transform: scale(0.125, 0.125);-moz-transform: scale(0.125, 0.125);transform: scale(0.125, 0.125);\"><\/path><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(3902599, document.getElementById('btnSpotPost'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"btnSettings\" class=\"IconSVG \" style=\"original_width:26 ;original_height:26 \n;z-index:10011;color:#000000;rotation:0;current_element_category_id:20881;left:902px;top:169px;current_element_id:1252;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" xmlns:a=\"http:\/\/ns.adobe.com\/AdobeSVGViewerExtensions\/3.0\/\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><g class=\"shape\" style=\"-webkit-transform: scale(1.2307692307692, 1.2307692307692);-moz-transform: scale(1.2307692307692, 1.2307692307692);transform: scale(1.2307692307692, 1.2307692307692);\"><defs><\/defs><rect display=\"none\" fill=\"#FFA400\" width=\"26\" height=\"26\"><\/rect><rect id=\"_x3C_Slice_x3E__109_\" display=\"none\" fill=\"none\" width=\"26\" height=\"26\"><\/rect><path fill='#000000' fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M22.703,13.191l3.294-1.374l-1.004-4.225L21.3,7.988 c-0.373-0.629-0.805-1.203-1.29-1.717l1.364-3.315l-3.696-2.28l-2.306,2.867c-0.799-0.21-1.622-0.314-2.454-0.319l-1.445-3.219 L7.277,1.132L7.77,4.693C7.19,5.053,6.659,5.462,6.186,5.91L2.999,4.597L0.72,8.295l2.743,2.218c-0.2,0.755-0.318,1.538-0.335,2.327 l-3.124,1.298l1.007,4.23l3.487-0.375c0.403,0.7,0.875,1.334,1.422,1.893l-1.285,3.116L8.33,25.28l2.216-2.75 c0.752,0.187,1.518,0.291,2.3,0.299l1.312,3.167l4.223-1.007L18,21.396c0.653-0.398,1.255-0.854,1.773-1.368l3.237,1.334 l2.277-3.701l-2.865-2.312C22.595,14.644,22.692,13.92,22.703,13.191z M13,19.014c-3.308,0-5.989-2.692-5.989-6.014 S9.692,6.986,13,6.986S18.989,9.679,18.989,13S16.308,19.014,13,19.014z M13,10.99c-1.11,0-2.01,0.9-2.01,2.01 c0,1.11,0.9,2.01,2.01,2.01s2.01-0.899,2.01-2.01C15.01,11.891,14.11,10.99,13,10.99z\"><\/path><\/g><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(494362, document.getElementById('btnSettings'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"btnLurkerMode\" class=\"IconSVG \" style=\"original_width:32 ;original_height:32 \n;z-index:10012;color:#000000;rotation:0;current_element_category_id:20881;left:902px;top:77px;current_element_id:1286;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" xmlns:a=\"http:\/\/ns.adobe.com\/AdobeSVGViewerExtensions\/3.0\/\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><g class=\"shape\" style=\"-webkit-transform: scale(1, 1);-moz-transform: scale(1, 1);transform: scale(1, 1);\"><defs><\/defs><rect display=\"none\" fill=\"#FFA400\" width=\"32\" height=\"32\"><\/rect><rect id=\"_x3C_Slice_x3E__109_\" display=\"none\" fill=\"none\" width=\"32\" height=\"32\"><\/rect><path fill='#000000' fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M0,15v2c8,12,24,12,32,0v-2C24,3,8,3,0,15z M16,24c-4.418,0-8-3.582-8-8 s3.582-8,8-8c4.418,0,8,3.582,8,8S20.418,24,16,24z M16,11.5c0-0.519,0.105-1.009,0.268-1.473C16.177,10.023,16.092,10,16,10 c-3.313,0-6,2.687-6,6s2.686,6,6,6c3.314,0,6-2.687,6-6c0-0.092-0.022-0.177-0.027-0.267C21.509,15.895,21.019,16,20.5,16 C18.015,16,16,13.985,16,11.5z\"><\/path><\/g><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(3781894, document.getElementById('btnLurkerMode'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10018;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:867px;top:353px;font-family:Arial;border-width:0px;border-style:solid;width:118px;height:24px;position:absolute;\" id=\"lblZtookers\" ><p class=\"wysiwyg-font-type-Arvo\">Ztookers (10)<\/p><\/div>\n    <script>(function() {Preview.mElements.set(149217, document.getElementById('lblZtookers'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:15px;top:353px;z-index:10013;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:117px;height:24px;position:absolute;\" id=\"lnkSocialGroups\" ><p class=\"wysiwyg-font-type-Arvo\">Social Groups<\/p><\/div>\n    <script>(function() {Preview.mElements.set(3527237, document.getElementById('lnkSocialGroups'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:155px;top:353px;z-index:10014;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:125px;height:24px;position:absolute;\" id=\"lnkZtookersFeed\" ><p class=\"wysiwyg-font-type-Arvo\">Ztooker's Feeds<\/p><\/div>\n    <script>(function() {Preview.mElements.set(2437029, document.getElementById('lnkZtookersFeed'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:313px;top:353px;z-index:10015;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:77px;height:24px;position:absolute;\" id=\"lnkMyPosts\" ><p class=\"wysiwyg-font-type-Arvo\">My Posts<\/p><\/div>\n    <script>(function() {Preview.mElements.set(3255003, document.getElementById('lnkMyPosts'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:415px;top:353px;z-index:10016;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:115px;height:24px;position:absolute;\" id=\"lnkSpottedFeeds\" ><p class=\"wysiwyg-font-type-Arvo\">Spotted Feeds<\/p><\/div>\n    <script>(function() {Preview.mElements.set(1361103, document.getElementById('lnkSpottedFeeds'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:740px;top:353px;z-index:10017;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:108px;height:24px;position:absolute;\" id=\"lnkZtooking\" ><p class=\"wysiwyg-font-type-Arvo\">Ztooking ( ${stooks.stookList.size()} )<\/p><\/div>\n    <script>(function() {Preview.mElements.set(2827483, document.getElementById('lnkZtooking'), {\"interactive\":true,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElBox \" style=\"left:0px;top:345px;z-index:10005;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(191,147,77,0.75);background-image:data\/113147_120237_congruent_pentagon_brown.png;background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;width:995px;height:40px;position:absolute;background-image: url(data\/113147_120237_congruent_pentagon_brown.png);\" id=\"el1684143_ae20be4686412a5921e0579fd9444d40\" ><\/div>\n    <script>(function() {Preview.mElements.set(1684143, document.getElementById('el1684143_ae20be4686412a5921e0579fd9444d40'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElGroup \" style=\"z-index:10021;left:705px;top:385px;font-family:;border-width:0px;border-style:solid;width:17px;height:656px;position:absolute;\" id=\"el2460782_3d48acf8ee900bc279e3b0c6e7b4a3be\" >\n      <div id=\"el762864_4beef85802a7900416b110d45fac6430\" class=\"GeoSVGEl \" style=\"z-index:10020;color:rgba(191,147,77,0.75);stroke:3;element:ellipse;left:0px;top:639px;font-family:;border-width:0px;border-style:solid;width:17px;height:17px;position:absolute;-webkit-transform-origin: 8.5px 8.5px;-moz-transform-origin: 8.5px 8.5px;-ms-transform-origin: 8.5px 8.5px;-o-transform-origin: 8.5px 8.5px;\">\n <svg version=\"1.2\" width=\"100%\" height=\"100%\">\n  <ellipse class=\"shape\" cx=\"50\" cy=\"50\" rx=\"50\" ry=\"50\" fill=\"rgba(191,147,77,0.75)\" style=\"-webkit-transform: scale(0.17,0.17); -webkit-transform-origin: 0% 0%; -moz-transform: scale(0.17,0.17); -moz-transform-origin: 0% 0%; -ms-transform: scale(0.17,0.17); -ms-transform-origin: 0% 0%; -o-transform: scale(0.17,0.17); -o-transform-origin: 0% 0%;\" \/>\n <\/svg>\n<\/div><script>(function() {Preview.mElements.set(762864, document.getElementById('el762864_4beef85802a7900416b110d45fac6430'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElBox \" style=\"z-index:10019;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(191,147,77,0.75);background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;background-image:url('');padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;left:6px;top:0px;width:5px;height:640px;position:absolute;\" id=\"el2585818_079f015db7fb5808d297d68ca02d2092\" ><\/div>\n    <script>(function() {Preview.mElements.set(2585818, document.getElementById('el2585818_079f015db7fb5808d297d68ca02d2092'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <\/div>\n    <script>(function() {Preview.mElements.set(2460782, document.getElementById('el2460782_3d48acf8ee900bc279e3b0c6e7b4a3be'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElBox \" style=\"left:14px;top:400px;z-index:10022;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:rgba(222,218,217,0.5);border-style:solid;background-color:rgba(235,235,235,0);background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;width:400px;height:975px;position:absolute;\" id=\"paneBigger\" ><\/div>\n    <script>(function() {Preview.mElements.set(498096, document.getElementById('paneBigger'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10026;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:735px;top:830px;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;\" id=\"lblPopularEvents\" ><p class=\"wysiwyg-font-type-Arvo\">Popular Events<\/p><\/div>\n    <script>(function() {Preview.mElements.set(2673401, document.getElementById('lblPopularEvents'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:735px;top:400px;z-index:10024;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;\" id=\"lblPopularPeople\" ><p class=\"wysiwyg-font-type-Arvo\">Popular People<\/p><\/div>\n    <script>(function() {Preview.mElements.set(2918070, document.getElementById('lblPopularPeople'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:735px;top:616px;z-index:10025;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:150px;height:24px;position:absolute;\" id=\"lblPopularLocation\" ><p class=\"wysiwyg-font-type-Arvo\">Popular Locations<\/p><\/div>\n    <script>(function() {Preview.mElements.set(5389848, document.getElementById('lblPopularLocation'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElBox \" style=\"left:425px;top:400px;z-index:10023;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:rgba(222,218,217,0.5);border-style:solid;background-color:rgba(235,235,235,0);background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;color:#000000;font-size:14px;font-weight:normal;font-style:normal;font-family:Arial;text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;width:273px;height:975px;position:absolute;\" id=\"paneSmaller\" ><\/div>\n    <script>(function() {Preview.mElements.set(7265272, document.getElementById('paneSmaller'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10041;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:761px;top:985px;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblEvent5\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Event 5<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(579276, document.getElementById('lblEvent5'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:860px;z-index:10037;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblEvent1\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Event 1<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(5898848, document.getElementById('lblEvent1'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:890px;z-index:10038;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblEvent2\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Event 2<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(5683157, document.getElementById('lblEvent2'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:920px;z-index:10039;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblEvent3\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Event 3<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(41172, document.getElementById('lblEvent3'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:950px;z-index:10040;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblEvent4\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Event 4<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(1940600, document.getElementById('lblEvent4'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:525px;z-index:10030;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblPopular4\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Azucena Ambil<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(2045763, document.getElementById('lblPopular4'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:465px;z-index:10028;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblPopular2\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Candy Claire Bitongga<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(2297268, document.getElementById('lblPopular2'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:435px;z-index:10027;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblPopular1\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Theresa Leah Laurito<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(3827748, document.getElementById('lblPopular1'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:495px;z-index:10029;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblPopular3\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Winnie Anne Verzosa<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(6624788, document.getElementById('lblPopular3'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:560px;z-index:10031;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblPopular5\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Matinee La<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(7472692, document.getElementById('lblPopular5'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:771px;z-index:10036;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblLocation5\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Location 5<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(57203, document.getElementById('lblLocation5'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:676px;z-index:10033;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"~\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Location 2<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(1275685, document.getElementById('lblLocation2'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:736px;z-index:10035;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblLocation4\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Location 4<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(1322238, document.getElementById('lblLocation4'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:706px;z-index:10034;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblLocation3\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Location 3<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(4702536, document.getElementById('lblLocation3'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:761px;top:646px;z-index:10032;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:213px;height:21px;position:absolute;\" id=\"lblLocation1\" ><p class=\"wysiwyg-font-type-Asap\" style=\"font-size: 14px;\"><span style=\"color: rgb(130, 88, 29);\">Location 1<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(6249759, document.getElementById('lblLocation1'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElGroup \" style=\"z-index:10055;left:125px;top:430px;font-family:;border-width:0px;border-style:solid;visibility:hidden;width:625px;height:495px;position:absolute;\" id=\"el1203718_cc9543c22d98ff56e7aa2714e963dd74\" >\n      \n      <div class=\"ElImageWrapper \" id=\"el454048_537ebd0ca8dbb45e2f12589151aaf772\" style=\"z-index:10043;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:20px;top:56px;width:300px;height:353px;position:absolute;\" >\n                      <img class=\"ElImage \" src=\"data\/imagepixel.png\" \/>\n                <\/div>\n\n    <script>(function() {Preview.mElements.set(454048, document.getElementById('el454048_537ebd0ca8dbb45e2f12589151aaf772'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10045;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:335px;top:56px;font-family:Arial;border-width:0px;border-style:solid;width:50px;height:21px;position:absolute;\" id=\"el810973_c120c7f7b734a0d26b703ed27190ba50\" ><p style=\"font-family: Asap; text-align: start; line-height: normal; font-size: 14px;\"><span style=\"font-style: normal; color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0); font-weight: bold;\">Caption<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(810973, document.getElementById('el810973_c120c7f7b734a0d26b703ed27190ba50'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script><textarea  style=\"z-index:10044;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#ffffff;color:#474747;font-size:12px;font-family:Arial;text-align:left;background-image:url('');font-weight:regular;line-height:normal;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-o-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-moz-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);-webkit-box-shadow:inset 1px 1px 2px rgba(0, 0, 0, 0.1);left:335px;top:80px;width:267px;height:187px;position:absolute;\" id=\"el928834_19bc4cff189c78a1942d774ccbbf3be3\" class=\"ElTextarea \" ><\/textarea><script>(function() {Preview.mElements.set(928834, document.getElementById('el928834_19bc4cff189c78a1942d774ccbbf3be3'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el743961_4d485c3b2b57dec517d50e287527933c\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10051;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:335px;top:289px;font-family:;border-width:0px;border-style:solid;width:18px;height:21px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><g class=\"shape\" style=\"-webkit-transform: scale(0.0703125, 0.08203125);-moz-transform: scale(0.0703125, 0.08203125);transform: scale(0.0703125, 0.08203125);\"><path fill='rgba(135,76,16,0.6)' fill=\"#343434\" d=\"M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128 L128,128L128,128L128,128L128,128L128,128L128,128z M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128z M128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128L128,128z\"><\/path><path fill='rgba(135,76,16,0.6)' fill=\"#343434\" d=\"M165.665,128.566c2.933,0.467,7.132,1,12.597,1.666c5.398,0.633,11.064,1.334,16.996,2.1 c5.865,0.767,11.33,1.566,16.462,2.366c5.132,0.833,8.798,1.566,11.13,2.199c3.8,1.101,7.666,3.399,11.531,6.865 c3.865,3.499,7.398,7.531,10.664,12.097s5.932,9.298,7.931,14.229c1.999,4.866,2.999,9.331,2.999,13.396v63.184 c-1.066,0.467-2.266,1.2-3.666,2.3c-1.332,1.1-2.799,2.166-4.398,3.199s-3.065,1.933-4.465,2.699 c-1.467,0.766-2.666,1.133-3.6,1.133H16.021c-3.833,0-6.698-1.166-8.665-3.532c-1.966-2.333-4.399-4.266-7.332-5.799v-63.184 c0-4.065,1-8.53,3-13.396c2.033-4.932,4.632-9.597,7.864-14.063c3.199-4.465,6.765-8.497,10.697-12.096 c3.899-3.566,7.765-5.932,11.597-7.032c1.933-0.633,5.532-1.366,10.764-2.199c5.232-0.8,10.831-1.6,16.762-2.366 c5.932-0.766,11.597-1.467,17.062-2.1c5.432-0.666,9.631-1.199,12.563-1.666c-9.998-6.398-17.862-14.763-23.594-25.06 c-5.731-10.297-8.564-21.494-8.564-33.591c0-9.564,1.833-18.595,5.532-27.026c3.699-8.431,8.698-15.829,14.963-22.228 c6.265-6.332,13.596-11.397,22.061-15.096C109.163,1.866,118.161,0,127.758,0c9.597,0,18.628,1.866,27.126,5.565 c8.514,3.699,15.912,8.764,22.311,15.096c6.332,6.398,11.33,13.796,14.996,22.228s5.465,17.462,5.465,27.026 c0,11.897-2.799,23.027-8.464,33.425C183.527,113.737,175.662,122.168,165.665,128.566z\"><\/path><\/g><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(743961, document.getElementById('el743961_4d485c3b2b57dec517d50e287527933c'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"left:365px;top:284px;z-index:10048;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:237px;height:30px;position:absolute;\" id=\"el672685_1381a328331e1f990bd6cbcfe07cba9c\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(672685, document.getElementById('el672685_1381a328331e1f990bd6cbcfe07cba9c'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el1059797_a2b51fe081ea946f3b729a3a6d5110f3\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10052;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:331px;top:335px;font-family:;border-width:0px;border-style:solid;width:26px;height:23px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><path fill='rgba(135,76,16,0.6)' fill=\"#343434\" d=\"M218.354,90.682c0,8.954-1.307,17.483-3.921,25.554c-2.614,8.072-6.111,15.718-10.49,22.973l-60.716,105.909 c-4.379,7.255-9.444,10.882-15.195,10.882c-5.784,0-10.751-3.627-14.901-10.882L52.089,138.915 c-4.379-7.288-7.875-14.902-10.489-22.94c-2.647-8.006-3.954-16.437-3.954-25.293c0-12.483,2.353-24.28,7.059-35.325 c4.706-11.045,11.176-20.686,19.41-28.92c8.202-8.202,17.843-14.673,28.888-19.378C104.047,2.353,115.811,0,128.164,0 c12.515,0,24.247,2.353,35.162,7.059c10.979,4.705,20.521,11.176,28.69,19.378c8.171,8.234,14.607,17.875,19.28,28.92 C216.002,66.402,218.354,78.199,218.354,90.682z M128.164,135.222c6.078,0,11.895-1.144,17.384-3.463 c5.49-2.288,10.294-5.458,14.346-9.51c4.052-4.052,7.222-8.823,9.51-14.28c2.319-5.425,3.464-11.208,3.464-17.287 c0-6.078-1.145-11.895-3.464-17.385c-2.288-5.49-5.458-10.326-9.51-14.509c-4.052-4.15-8.791-7.385-14.247-9.672 c-5.457-2.288-11.274-3.464-17.483-3.464c-6.274,0-12.091,1.177-17.45,3.464c-5.359,2.287-10.098,5.522-14.248,9.672 c-4.183,4.183-7.385,8.986-9.706,14.444c-2.287,5.424-3.431,11.241-3.431,17.45c0,6.079,1.144,11.862,3.431,17.287 c2.32,5.457,5.523,10.228,9.706,14.28c4.15,4.052,8.888,7.222,14.248,9.51C116.072,134.078,121.889,135.222,128.164,135.222z\" class=\"shape\" style=\"-webkit-transform: scale(0.1015625, 0.08984375);-moz-transform: scale(0.1015625, 0.08984375);transform: scale(0.1015625, 0.08984375);\"><\/path><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(1059797, document.getElementById('el1059797_a2b51fe081ea946f3b729a3a6d5110f3'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"left:365px;top:331px;z-index:10049;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:237px;height:30px;position:absolute;\" id=\"el225796_7788de14b491f919805badda596a955b\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(225796, document.getElementById('el225796_7788de14b491f919805badda596a955b'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el891639_123d77ada58344497aca6e1f34c59b0e\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10053;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:335px;top:380px;font-family:;border-width:0px;border-style:solid;width:22px;height:27px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><path fill='rgba(135,76,16,0.6)' fill=\"#343434\" d=\"M256,114.163c0,4.874-1.53,9.18-4.534,12.921c-3.003,3.684-6.772,6.063-11.334,7.197v51.228 c0,5.837-2.097,10.88-6.29,15.018c-4.222,4.193-9.237,6.232-15.073,6.232c-6.291-6.289-13.884-12.354-22.781-18.304 c-8.896-5.949-18.474-11.39-28.674-16.32c-10.258-4.93-20.798-9.18-31.621-12.693c-10.824-3.514-21.336-5.837-31.508-6.97 c-3.995,1.133-7.253,2.945-9.747,5.553c-2.493,2.606-4.193,5.497-5.129,8.671c-0.935,3.23-1.02,6.459-0.283,9.803 c0.766,3.344,2.466,6.348,5.129,9.011c-2.295,3.796-3.372,7.31-3.174,10.54c0.17,3.174,1.134,6.234,2.834,9.236 c1.7,2.947,4.023,5.781,6.886,8.501c2.86,2.721,5.921,5.44,9.151,8.16c-1.842,3.967-5.044,6.971-9.634,9.067 s-9.492,3.287-14.733,3.57c-5.214,0.283-10.343-0.227-15.357-1.474c-4.986-1.247-8.925-3.287-11.787-6.063 c-1.672-5.61-3.514-11.391-5.554-17.341c-2.039-5.949-3.711-11.957-4.986-18.134c-1.304-6.177-1.983-12.523-1.983-19.154 c0-6.63,1.134-13.657,3.344-21.08H21.392c-5.836,0-10.88-2.04-15.073-6.233C2.096,140.967,0,135.924,0,129.974V98.183 c0-5.837,2.067-10.881,6.262-15.073c4.137-4.251,9.181-6.348,15.13-6.348h69.306c10.654,0,21.987-1.53,34.029-4.646 c12.042-3.117,23.829-7.253,35.418-12.467c11.588-5.271,22.47-11.164,32.726-17.794c10.201-6.631,18.871-13.431,25.898-20.515 c5.836,0,10.852,2.097,15.073,6.291c4.193,4.137,6.29,9.18,6.29,15.13v51.115c4.562,1.077,8.331,3.514,11.334,7.254 C254.47,104.926,256,109.233,256,114.163z M218.769,49.562c-7.027,5.439-14.733,10.71-23.064,15.754 c-8.33,5.043-17.171,9.633-26.464,13.657c-9.294,4.08-18.813,7.706-28.477,10.767c-9.661,3.116-19.239,5.384-28.674,6.743v35.305 c9.435,1.474,19.013,3.74,28.674,6.8c9.663,3.061,19.183,6.688,28.477,10.824c9.293,4.193,18.189,8.783,26.605,13.771 s16.065,10.199,22.923,15.526V49.562z\" class=\"shape\" style=\"-webkit-transform: scale(0.0859375, 0.10546875);-moz-transform: scale(0.0859375, 0.10546875);transform: scale(0.0859375, 0.10546875);\"><\/path><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(891639, document.getElementById('el891639_123d77ada58344497aca6e1f34c59b0e'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"z-index:10050;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-size:auto;background-repeat:repeat;background-position:center center;background-image:url('');box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;left:365px;top:378px;width:237px;height:30px;position:absolute;\" id=\"el913519_94b536f062ad386f9ffd425ac44b752e\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(913519, document.getElementById('el913519_94b536f062ad386f9ffd425ac44b752e'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <input type=\"button\" style=\";left:29px;top:440px;z-index:10046;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;\" class=\"ElButton active \" value=\"Post\" id=\"el590169_9cdb898c7fbbc3c1fb161e86f6d4e722\" \/>\n    <script>(function() {Preview.mElements.set(590169, document.getElementById('el590169_9cdb898c7fbbc3c1fb161e86f6d4e722'), {\"interactive\":true,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <input type=\"button\" style=\";z-index:10047;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;left:477px;top:440px;width:120px;height:30px;position:absolute;;\" class=\"ElButton active \" value=\"Cancel\" id=\"el204524_e64b8e347a22c09492f4f827dfab4765\" \/>\n    <script>(function() {Preview.mElements.set(204524, document.getElementById('el204524_e64b8e347a22c09492f4f827dfab4765'), {\"interactive\":true,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el1511728_9240331592a7b33a54de5686bd7f43b9\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10054;color:rgba(135,76,16,0.6);rotation:0;current_element_category_id:20973;left:161px;top:445px;font-family:;border-width:0px;border-style:solid;width:19px;height:19px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><path fill='rgba(135,76,16,0.6)' fill=\"#343434\" d=\"M44.667,74.854c3.361,0,5.972,0.861,7.833,2.639s3.222,3.889,4,6.333c0.833,2.444,1.333,5.027,1.5,7.694 c0.194,2.667,0.278,4.833,0.278,6.472v20.695c-1.278,0.722-2.333,1.555-3.195,2.5c-0.861,0.972-2.111,1.444-3.75,1.444H6.667 c-1.445,0-2.639-0.473-3.611-1.444c-0.944-0.945-1.944-1.778-3.056-2.5V97.993c0-1.639,0.056-3.805,0.139-6.472 c0.083-2.667,0.556-5.25,1.361-7.694c0.833-2.444,2.167-4.556,4.028-6.333s4.472-2.639,7.805-2.639 c-4.055-2.639-7.305-6.083-9.722-10.305C1.222,60.326,0,55.576,0,50.326c0-3.917,0.778-7.639,2.25-11.167 c1.5-3.527,3.583-6.639,6.25-9.333c2.694-2.667,5.806-4.75,9.333-6.25c3.556-1.5,7.278-2.25,11.167-2.25 c4.111,0,7.917,0.75,11.444,2.25c3.556,1.5,6.611,3.583,9.194,6.25c2.583,2.694,4.694,5.806,6.25,9.333 c1.611,3.528,2.389,7.25,2.389,11.167c0,5.167-1.222,9.917-3.722,14.167S48.778,72.215,44.667,74.854z M215.277,140.465 c6,5.361,10.723,10.667,14.111,15.889c3.389,5.209,5.111,11.153,5.111,17.875v52.723c-1.111,0.5-2,1.111-2.723,1.777 c-0.722,0.611-1.555,1.223-2.527,1.889c-0.973,0.611-2.027,1.278-3.194,1.945c-1.167,0.722-2.833,1.389-4.889,2.111H34.861 c-3.167,0-5.639-1-7.417-3.057c-1.777-2.055-3.722-3.61-5.917-4.666v-52.723c0-7.055,2.028-13.333,6.139-18.847 c4.055-5.473,8.5-10.473,13.333-14.917c0.889-0.889,2.056-1.889,3.472-2.916c1.417-1.057,2.917-1.75,4.556-2.111 c1.639-0.556,3.5-0.861,5.583-0.973c2.083-0.083,4.111-0.306,6.111-0.666c5.556-0.89,11.445-1.778,17.722-2.667 c6.278-0.833,12.389-1.723,18.389-2.639c-8.166-5.194-14.722-12.055-19.666-20.639c-4.973-8.555-7.445-18-7.445-28.25 c0-7.972,1.556-15.527,4.639-22.666c3.083-7.139,7.25-13.306,12.527-18.528c5.278-5.222,11.444-9.361,18.5-12.444 C112.5,22.854,120,21.326,128,21.326s15.527,1.527,22.611,4.639c7.083,3.083,13.25,7.223,18.527,12.444 c5.25,5.223,9.417,11.389,12.528,18.528c3.083,7.139,4.61,14.694,4.61,22.666c0,10.25-2.444,19.639-7.333,28.195 c-4.917,8.528-11.5,15.444-19.75,20.694c5.973,0.917,12.083,1.75,18.306,2.583c6.223,0.833,12.167,1.723,17.777,2.723 c2.084,0.36,4.111,0.583,6.111,0.666c2,0.111,3.889,0.417,5.611,0.973c1.611,0.361,3.139,1.055,4.556,2.111 C212.944,138.576,214.223,139.576,215.277,140.465z M242.667,74.854c3.36,0,5.916,0.861,7.694,2.639 c1.75,1.778,3.056,3.889,3.861,6.333c0.833,2.444,1.333,5.027,1.5,7.694c0.194,2.667,0.277,4.833,0.277,6.472v20.695 c-1.083,0.722-2.111,1.555-3.056,2.5c-0.944,0.972-2.167,1.444-3.611,1.444h-44.666c-1.611,0-2.89-0.473-3.723-1.444 c-0.889-0.945-1.944-1.778-3.222-2.5V97.993c0-1.639,0.11-3.805,0.277-6.472s0.723-5.25,1.556-7.694 c0.889-2.444,2.222-4.556,4.111-6.333c1.833-1.778,4.416-2.639,7.666-2.639c-4.056-2.639-7.36-6.083-9.86-10.305 s-3.75-8.972-3.75-14.222c0-3.917,0.75-7.639,2.25-11.167c1.5-3.527,3.583-6.639,6.25-9.333c2.694-2.667,5.805-4.75,9.333-6.25 c3.556-1.5,7.361-2.25,11.444-2.25c3.889,0,7.611,0.75,11.167,2.25s6.666,3.583,9.333,6.25c2.667,2.694,4.777,5.806,6.277,9.333 c1.5,3.528,2.223,7.25,2.223,11.167c0,5.167-1.194,9.917-3.611,14.167C250,68.743,246.75,72.215,242.667,74.854z\" class=\"shape\" style=\"-webkit-transform: scale(0.07421875, 0.07421875);-moz-transform: scale(0.07421875, 0.07421875);transform: scale(0.07421875, 0.07421875);\"><\/path><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(1511728, document.getElementById('el1511728_9240331592a7b33a54de5686bd7f43b9'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>  \r\n<div class=\"modal-overlay\" style=\"display: none;z-index: 10042\"><\/div>\r\n\r\n<div class=\"ElDialog \" style=\"z-index:10042;border-width:1px;border-radius:0px;border-color:#c7c7c7;border-style:solid;background-color:#F7F5F5;left:0px;top:0px;font-family:;width:623px;height:493px;position:absolute\" id=\"el553773_7f6575a8972e337bb887226a9dd689e7\" >\r\n  <div class=\"topbar\" style=\"color:#e6b907;font-weight:bold;font-style:normal;font-size:14px;font-family:Trebuchet MS;text-align:none;background-color:#333025;display:block;height: 24px;line-height: 24px\">\r\n    <div class=\"text\">Tell us about what you&#x27;ve seen<\/div>\r\n  <\/div>\r\n  <div class=\"close\">&times;<\/div>\r\n  <div class=\"content\" style=\"color:#666;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;background-color:rgba(250,229,0,0.3)\">\r\n    <div class=\"text\" style=\"padding-top: 54px\"><\/div>\r\n  <\/div>\r\n  <div class=\"bottombar\" style=\"background-color:#ffffff;display:none\"><\/div>\r\n<\/div>\r<script>(function() {Preview.mElements.set(553773, document.getElementById('el553773_7f6575a8972e337bb887226a9dd689e7'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <\/div>\n    <script>(function() {Preview.mElements.set(1203718, document.getElementById('el1203718_cc9543c22d98ff56e7aa2714e963dd74'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElGroup \" style=\"z-index:10076;left:164px;top:377px;font-family:;border-width:0px;border-style:solid;visibility:hidden;width:503px;height:629px;position:absolute;\" id=\"el2689083_1768d25daa4c20c61c3a02ea130fc70f\" >\n          <div class=\"ElTextElement \" style=\"left:21px;top:51px;z-index:10060;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:75px;height:24px;position:absolute;\" id=\"el5900875_d6c305b60ee34c6f6d440ec14b295850\" ><p class=\"wysiwyg-font-type-Arvo\"><span style=\"font-weight: bold;\">Account<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(5900875, document.getElementById('el5900875_d6c305b60ee34c6f6d440ec14b295850'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>  \r\n<div class=\"modal-overlay\" style=\"display: none;z-index: 10057\"><\/div>\r\n\r\n<div class=\"ElDialog \" style=\"left:0px;top:0px;z-index:10057;border-width:1px;border-radius:0px;border-color:#c7c7c7;border-style:solid;background-color:#F7F5F5;font-family:;width:501px;height:627px;position:absolute\" id=\"el1086239_c2c08e43cab966b8e217d32e810d3c0f\" >\r\n  <div class=\"topbar\" style=\"color:#e6b907;font-weight:bold;font-style:normal;font-size:14px;font-family:Trebuchet MS;text-align:none;background-color:#333025;display:block;height: 24px;line-height: 24px\">\r\n    <div class=\"text\">Change your personal settings here<\/div>\r\n  <\/div>\r\n  <div class=\"close\">&times;<\/div>\r\n  <div class=\"content\" style=\"color:#666;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;background-color:rgba(250,229,0,0.3)\">\r\n    <div class=\"text\" style=\"padding-top: 54px\"><\/div>\r\n  <\/div>\r\n  <div class=\"bottombar\" style=\"background-color:#ffffff;display:none\"><\/div>\r\n<\/div>\r<script>(function() {Preview.mElements.set(1086239, document.getElementById('el1086239_c2c08e43cab966b8e217d32e810d3c0f'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"left:141px;top:91px;z-index:10062;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el7047278_4a2c84b7f57734ec8fc7fbf875813303\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(7047278, document.getElementById('el7047278_4a2c84b7f57734ec8fc7fbf875813303'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:36px;top:96px;z-index:10063;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:99px;height:23px;position:absolute;\" id=\"el8388672_ec4d7eeba9e65b7bd490ee9e0f11acc1\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">Email<\/p><\/div>\n    <script>(function() {Preview.mElements.set(8388672, document.getElementById('el8388672_ec4d7eeba9e65b7bd490ee9e0f11acc1'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:36px;top:157px;z-index:10065;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:105px;height:23px;position:absolute;\" id=\"el6069034_7be7f946236246b3411bc95f69b8109e\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">First Name<\/p><\/div>\n    <script>(function() {Preview.mElements.set(6069034, document.getElementById('el6069034_7be7f946236246b3411bc95f69b8109e'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"left:141px;top:151px;z-index:10064;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el5639657_4086be1addd6bc4617488420e6abbb77\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(5639657, document.getElementById('el5639657_4086be1addd6bc4617488420e6abbb77'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:36px;top:212px;z-index:10067;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:105px;height:23px;position:absolute;\" id=\"el8246124_0ea7b0d13f1916a330d051502ed510e6\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">Last Name<\/p><\/div>\n    <script>(function() {Preview.mElements.set(8246124, document.getElementById('el8246124_0ea7b0d13f1916a330d051502ed510e6'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"text\" class=\" \" style=\"left:141px;top:206px;z-index:10066;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el3073017_686b331d4f21179abd9c4b137ad05d28\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(3073017, document.getElementById('el3073017_686b331d4f21179abd9c4b137ad05d28'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:36px;top:270px;z-index:10068;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:23px;position:absolute;\" id=\"el103019_969fe40ebe5f3950cc18f66663f7cdc0\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">Country<\/p><\/div>\n    <script>(function() {Preview.mElements.set(103019, document.getElementById('el103019_969fe40ebe5f3950cc18f66663f7cdc0'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <select class=\"\" style=\"left:143px;top:265px;multiple:disabled;z-index:10075;background-color:#ffffff;border-width:1px;border-radius:0px;border-color:#c8c8c8;border-style:solid;color:#666;font-size:14px;font-family:Arial;text-align:left;width:303px;height:31px;position:absolute;\" id=\"el2673762_b0207162b920a326bfe44e35fad69f7c\" ><option value=\"USA\">USA<\/option><option value=\"Philippines\">Philippines<\/option><option value=\"Indonesia\">Indonesia<\/option><\/select><script>(function() {Preview.mElements.set(2673762, document.getElementById('el2673762_b0207162b920a326bfe44e35fad69f7c'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:21px;top:324px;z-index:10061;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:145px;height:24px;position:absolute;\" id=\"el803660_7d00e4b5ea832661551b0195964b3a49\" ><p class=\"wysiwyg-font-type-Arvo\"><span style=\"font-weight: bold;\">Change Password<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(803660, document.getElementById('el803660_7d00e4b5ea832661551b0195964b3a49'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:41px;top:376px;z-index:10072;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:41px;position:absolute;\" id=\"el7899589_18dac0e03509fc1141ea28b44b7e2fd7\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">Current Password<\/p><\/div>\n    <script>(function() {Preview.mElements.set(7899589, document.getElementById('el7899589_18dac0e03509fc1141ea28b44b7e2fd7'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"password\" class=\" \" style=\"left:141px;top:376px;z-index:10071;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el5333821_1882779b32e999e27564e50e08b73739\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(5333821, document.getElementById('el5333821_1882779b32e999e27564e50e08b73739'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:41px;top:445px;z-index:10070;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:23px;position:absolute;\" id=\"el4580197_78c6204937f5ecf4da622e1ae4cba83b\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">New Password<\/p><\/div>\n    <script>(function() {Preview.mElements.set(4580197, document.getElementById('el4580197_78c6204937f5ecf4da622e1ae4cba83b'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"password\" class=\" \" style=\"left:141px;top:439px;z-index:10069;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el5942999_316fe21fcdb2594f1aa26dbadbcb6c3c\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(5942999, document.getElementById('el5942999_316fe21fcdb2594f1aa26dbadbcb6c3c'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"left:41px;top:499px;z-index:10074;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;font-family:Arial;border-width:0px;border-style:solid;width:100px;height:41px;position:absolute;\" id=\"el2375065_aaf24125747983616a4a05e303b6da90\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 14px;\">Confirm Password<\/p><\/div>\n    <script>(function() {Preview.mElements.set(2375065, document.getElementById('el2375065_aaf24125747983616a4a05e303b6da90'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElInputWithoutLabel\">\n      <input   type=\"password\" class=\" \" style=\"left:141px;top:499px;z-index:10073;font-weight:normal;font-style:normal;font-size:14px;font-family:Arial;text-align:none;color:#474747;inactive:0;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:#fff;background-image:url('');background-size:auto;background-repeat:repeat;background-position:center center;box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-o-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-moz-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;-webkit-box-shadow:rgba(0, 0, 0, 0.1) 1px 1px 1px inset;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;width:300px;height:30px;position:absolute;\" id=\"el5708752_53fb344b0b078ac7cc6a63a1eb437ca9\"  value=\"\">\n    <\/div>\n    <script>(function() {Preview.mElements.set(5708752, document.getElementById('el5708752_53fb344b0b078ac7cc6a63a1eb437ca9'), {\"interactive\":false,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <input type=\"button\" style=\";left:41px;top:570px;z-index:10058;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;\" class=\"ElButton active \" value=\"Save\" id=\"el6017404_bf8400d657f090d3d755e7bb96063f1a\" \/>\n    <script>(function() {Preview.mElements.set(6017404, document.getElementById('el6017404_bf8400d657f090d3d755e7bb96063f1a'), {\"interactive\":true,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <input type=\"button\" style=\";left:338px;top:570px;z-index:10059;border-top-width:0px;border-bottom-width:0px;border-left-width:0px;border-right-width:0px;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;border-color:#dddddd;border-style:solid;background-color:#3498db;background-image:url('');color:#FFF;font-size:14px;font-weight:bold;font-style:normal;font-family:Arial;text-align:center;box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-o-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);-webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.4);background-size:auto;background-repeat:repeat;background-position:center center;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;width:120px;height:30px;position:absolute;;\" class=\"ElButton active \" value=\"Cancel\" id=\"el7997034_134e3dcbb416c0bbc9cbb7aff41b44be\" \/>\n    <script>(function() {Preview.mElements.set(7997034, document.getElementById('el7997034_134e3dcbb416c0bbc9cbb7aff41b44be'), {\"interactive\":true,\"in_group\":true,\"in_component\":false,\"id_component\":false})})();<\/script>    <\/div>\n    <script>(function() {Preview.mElements.set(2689083, document.getElementById('el2689083_1768d25daa4c20c61c3a02ea130fc70f'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>\n<script>\nInteractions.ori_styles = {\"2646256\":{\"opacity\":null,\"width\":995,\"height\":345,\"top\":0,\"left\":0},\"2223556\":{\"opacity\":null,\"width\":260,\"height\":245,\"top\":63,\"left\":597},\"351375\":{\"opacity\":null,\"width\":300,\"height\":109,\"top\":118,\"left\":80},\"1986503\":{\"opacity\":null,\"width\":279,\"height\":265,\"top\":53,\"left\":588},\"599320\":{\"opacity\":null,\"width\":300,\"height\":24,\"top\":223,\"left\":80},\"3552193\":{\"opacity\":null,\"width\":52,\"height\":52,\"top\":247,\"left\":892},\"2323527\":{\"opacity\":null,\"width\":52,\"height\":52,\"top\":67,\"left\":892},\"4075710\":{\"opacity\":null,\"width\":52,\"height\":52,\"top\":159,\"left\":892},\"3902599\":{\"opacity\":null,\"width\":32,\"height\":32,\"top\":256,\"left\":902,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"},\"494362\":{\"opacity\":null,\"width\":32,\"height\":32,\"top\":169,\"left\":902,\"original_width\":\"26 \",\"original_height\":\"26 \\n\"},\"3781894\":{\"opacity\":null,\"width\":32,\"height\":32,\"top\":77,\"left\":902,\"original_width\":\"32 \",\"original_height\":\"32 \\n\"},\"149217\":{\"opacity\":null,\"width\":118,\"height\":24,\"top\":353,\"left\":867},\"3527237\":{\"opacity\":null,\"width\":117,\"height\":24,\"top\":353,\"left\":15},\"2437029\":{\"opacity\":null,\"width\":125,\"height\":24,\"top\":353,\"left\":155},\"3255003\":{\"opacity\":null,\"width\":77,\"height\":24,\"top\":353,\"left\":313},\"1361103\":{\"opacity\":null,\"width\":115,\"height\":24,\"top\":353,\"left\":415},\"2827483\":{\"opacity\":null,\"width\":108,\"height\":24,\"top\":353,\"left\":740},\"1684143\":{\"opacity\":null,\"width\":995,\"height\":40,\"top\":345,\"left\":0},\"2460782\":{\"opacity\":null,\"width\":17,\"height\":656,\"top\":385,\"left\":705},\"762864\":{\"opacity\":null,\"width\":17,\"height\":17,\"top\":639,\"left\":0},\"2585818\":{\"opacity\":null,\"width\":5,\"height\":640,\"top\":0,\"left\":6},\"498096\":{\"opacity\":null,\"width\":400,\"height\":975,\"top\":400,\"left\":14},\"2673401\":{\"opacity\":null,\"width\":150,\"height\":24,\"top\":830,\"left\":735},\"2918070\":{\"opacity\":null,\"width\":150,\"height\":24,\"top\":400,\"left\":735},\"5389848\":{\"opacity\":null,\"width\":150,\"height\":24,\"top\":616,\"left\":735},\"7265272\":{\"opacity\":null,\"width\":273,\"height\":975,\"top\":400,\"left\":425},\"579276\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":985,\"left\":761},\"5898848\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":860,\"left\":761},\"5683157\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":890,\"left\":761},\"41172\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":920,\"left\":761},\"1940600\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":950,\"left\":761},\"2045763\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":525,\"left\":761},\"2297268\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":465,\"left\":761},\"3827748\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":435,\"left\":761},\"6624788\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":495,\"left\":761},\"7472692\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":560,\"left\":761},\"57203\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":771,\"left\":761},\"1275685\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":676,\"left\":761},\"1322238\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":736,\"left\":761},\"4702536\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":706,\"left\":761},\"6249759\":{\"opacity\":null,\"width\":213,\"height\":21,\"top\":646,\"left\":761},\"1203718\":{\"opacity\":null,\"width\":625,\"height\":495,\"top\":430,\"left\":125},\"454048\":{\"opacity\":null,\"width\":300,\"height\":353,\"top\":56,\"left\":20},\"810973\":{\"opacity\":null,\"width\":50,\"height\":21,\"top\":56,\"left\":335},\"928834\":{\"opacity\":null,\"width\":267,\"height\":187,\"top\":80,\"left\":335},\"743961\":{\"opacity\":null,\"width\":18,\"height\":21,\"top\":289,\"left\":335,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"},\"672685\":{\"opacity\":null,\"width\":237,\"height\":30,\"top\":284,\"left\":365},\"1059797\":{\"opacity\":null,\"width\":26,\"height\":23,\"top\":335,\"left\":331,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"},\"225796\":{\"opacity\":null,\"width\":237,\"height\":30,\"top\":331,\"left\":365},\"891639\":{\"opacity\":null,\"width\":22,\"height\":27,\"top\":380,\"left\":335,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"},\"913519\":{\"opacity\":null,\"width\":237,\"height\":30,\"top\":378,\"left\":365},\"590169\":{\"opacity\":null,\"width\":120,\"height\":30,\"top\":440,\"left\":29},\"204524\":{\"opacity\":null,\"width\":120,\"height\":30,\"top\":440,\"left\":477},\"1511728\":{\"opacity\":null,\"width\":19,\"height\":19,\"top\":445,\"left\":161,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"},\"553773\":{\"opacity\":null,\"width\":625,\"height\":495,\"top\":0,\"left\":0},\"2689083\":{\"opacity\":null,\"width\":503,\"height\":629,\"top\":377,\"left\":164},\"5900875\":{\"opacity\":null,\"width\":75,\"height\":24,\"top\":51,\"left\":21},\"1086239\":{\"opacity\":null,\"width\":503,\"height\":629,\"top\":0,\"left\":0},\"7047278\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":91,\"left\":141},\"8388672\":{\"opacity\":null,\"width\":99,\"height\":23,\"top\":96,\"left\":36},\"6069034\":{\"opacity\":null,\"width\":105,\"height\":23,\"top\":157,\"left\":36},\"5639657\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":151,\"left\":141},\"8246124\":{\"opacity\":null,\"width\":105,\"height\":23,\"top\":212,\"left\":36},\"3073017\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":206,\"left\":141},\"103019\":{\"opacity\":null,\"width\":100,\"height\":23,\"top\":270,\"left\":36},\"2673762\":{\"opacity\":null,\"width\":305,\"height\":33,\"top\":265,\"left\":143},\"803660\":{\"opacity\":null,\"width\":145,\"height\":24,\"top\":324,\"left\":21},\"7899589\":{\"opacity\":null,\"width\":100,\"height\":41,\"top\":376,\"left\":41},\"5333821\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":376,\"left\":141},\"4580197\":{\"opacity\":null,\"width\":100,\"height\":23,\"top\":445,\"left\":41},\"5942999\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":439,\"left\":141},\"2375065\":{\"opacity\":null,\"width\":100,\"height\":41,\"top\":499,\"left\":41},\"5708752\":{\"opacity\":null,\"width\":300,\"height\":30,\"top\":499,\"left\":141},\"6017404\":{\"opacity\":null,\"width\":120,\"height\":30,\"top\":570,\"left\":41},\"7997034\":{\"opacity\":null,\"width\":120,\"height\":30,\"top\":570,\"left\":338}};\nInteractions.scrollable_el = document.getElementById('canvas-wrapper');\n\n      document.getElementById('btnSpotPost') && document.getElementById('btnSpotPost').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '1203718';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"slide\",\"time\":\"300\",\"style\":\"ease-in\"};\n  var is_scroll = false;\n  var is_keydown = false;  \n  var event_value = null;\n\n  if (\n    (is_scroll && 0 === Interactions.ifFireOneTimeScroll(id, 'visibility', event_value)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  if (el.isVisible()) {\n    el.hide(animation);\n  }\n  else {\n    el.show(animation);\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('btnSettings') && document.getElementById('btnSettings').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 494362;\n  var animation = null;\n  var styles = {\"color\":\"#f5ce0a\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('btnSettings') && document.getElementById('btnSettings').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 494362;\n  var animation = null;\n  var styles = {\"color\":\"#000000\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('btnSettings') && document.getElementById('btnSettings').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '2689083';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"slide\",\"time\":\"300\",\"style\":\"ease-in\"};\n  var is_scroll = false;\n  var is_keydown = false;  \n  var event_value = null;\n\n  if (\n    (is_scroll && 0 === Interactions.ifFireOneTimeScroll(id, 'visibility', event_value)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  if (el.isVisible()) {\n    el.hide(animation);\n  }\n  else {\n    el.show(animation);\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('btnLurkerMode') && document.getElementById('btnLurkerMode').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 3781894;\n  var animation = null;\n  var styles = {\"color\":\"#29cc04\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('lnkSocialGroups') && document.getElementById('lnkSocialGroups').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 3527237;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":\"#ffffff\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkSocialGroups') && document.getElementById('lnkSocialGroups').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 3527237;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":[]};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkSocialGroups') && document.getElementById('lnkSocialGroups').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function() {\n  var action = {\"target\":false,\"url\":\"\\\/groups\"};\n\n  if (action.target) {\n    window.open(action.url, action.target);\n  }\n  else {\n    window.location.href = action.url;\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('lnkZtookersFeed') && document.getElementById('lnkZtookersFeed').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 2437029;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":\"#ffffff\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkZtookersFeed') && document.getElementById('lnkZtookersFeed').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 2437029;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":[]};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkZtookersFeed') && document.getElementById('lnkZtookersFeed').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function() {\n  var action = {\"target\":false,\"url\":\"\\\/ztookerfeeds\"};\n\n  if (action.target) {\n    window.open(action.url, action.target);\n  }\n  else {\n    window.location.href = action.url;\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('lnkMyPosts') && document.getElementById('lnkMyPosts').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 3255003;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":\"#ffffff\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkMyPosts') && document.getElementById('lnkMyPosts').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 3255003;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":[]};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkMyPosts') && document.getElementById('lnkMyPosts').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function() {\n  var action = {\"target\":false,\"url\":\"\\\/postfeeds\"};\n\n  if (action.target) {\n    window.open(action.url, action.target);\n  }\n  else {\n    window.location.href = action.url;\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('lnkSpottedFeeds') && document.getElementById('lnkSpottedFeeds').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 1361103;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":\"#ffffff\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkSpottedFeeds') && document.getElementById('lnkSpottedFeeds').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 1361103;\n  var animation = {\"style\":\"linear\",\"time\":\"300\",\"show\":\"\"};\n  var styles = {\"color\":[]};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkSpottedFeeds') && document.getElementById('lnkSpottedFeeds').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function() {\n  var action = {\"target\":false,\"url\":\"\\\/spottedfeeds\"};\n\n  if (action.target) {\n    window.open(action.url, action.target);\n  }\n  else {\n    window.location.href = action.url;\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('lnkZtooking') && document.getElementById('lnkZtooking').addEventListener('mouseenter', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 2827483;\n  var animation = {\"time\":\"300\",\"show\":\"\",\"style\":\"linear\"};\n  var styles = {\"color\":\"#ffffff\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkZtooking') && document.getElementById('lnkZtooking').addEventListener('mouseleave', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = 2827483;\n  var animation = {\"time\":\"300\",\"show\":\"\",\"style\":\"linear\"};\n  var styles = {\"color\":[]};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = true;\n  var event_value = null;\n\n  if (is_scroll) {\n    var fire_factor = Interactions.ifFireOneTimeScroll(id, 'style', event_value);\n\n    if (fire_factor === 0) {\n      return false;\n    }\n\n    if (fire_factor === -1) {\n      var ori_styles = Interactions.ori_styles[id];\n\n      for (var style in styles) {\n        if (styles.hasOwnProperty(style)) {\n          styles[style] = ori_styles[style];\n        }\n      }\n    }\n  }\n  else if (is_keydown && e.keyCode !== parseInt(event_value)) {\n    return false;\n  }\n\n  \/\/ set one animation for all changed styles\n  Preview.mElements.get(id).setAnimation('style', animation);\n\n  for (var style in styles) {\n    if (styles.hasOwnProperty(style)) {\n      var style_value = styles[style];\n      var update_original = true;\n\n      if (is_reverse && Interactions.ori_styles[id][style]) {\n        style_value = Interactions.ori_styles[id][style];\n        update_original = false;\n      }\n\n      Preview.mElements.get(id).setStyle(style, style_value, update_original, false);\n    }\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n      document.getElementById('lnkZtooking') && document.getElementById('lnkZtooking').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function() {\n  var action = {\"target\":false,\"url\":\"\\\/ztooking\"};\n\n  if (action.target) {\n    window.open(action.url, action.target);\n  }\n  else {\n    window.location.href = action.url;\n  }\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('el590169_9cdb898c7fbbc3c1fb161e86f6d4e722') && document.getElementById('el590169_9cdb898c7fbbc3c1fb161e86f6d4e722').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '1203718';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"slide\",\"time\":\"300\",\"style\":\"ease-out\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (\n    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  el.hide(animation);\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('el204524_e64b8e347a22c09492f4f827dfab4765') && document.getElementById('el204524_e64b8e347a22c09492f4f827dfab4765').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '1203718';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"fade\",\"time\":\"300\",\"style\":\"ease-out\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (\n    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  el.hide(animation);\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('el6017404_bf8400d657f090d3d755e7bb96063f1a') && document.getElementById('el6017404_bf8400d657f090d3d755e7bb96063f1a').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '2689083';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"slide\",\"time\":\"300\",\"style\":\"ease-out\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (\n    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  el.hide(animation);\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n        document.getElementById('el7997034_134e3dcbb416c0bbc9cbb7aff41b44be') && document.getElementById('el7997034_134e3dcbb416c0bbc9cbb7aff41b44be').addEventListener('click', function(e) {\n      var processed = [];\n\n                        processed.push((function(e) {\n  var id = '2689083';\n  var el = Preview.mElements.get(id);\n  var animation = {\"show\":\"fade\",\"time\":\"300\",\"style\":\"linear\"};\n  var is_scroll = false;\n  var is_keydown = false;\n  var is_reverse = false;\n  var event_value = null;\n\n  if (\n    (is_scroll && 1 !== Interactions.ifFireOneTimeScroll(id, 'visibility', event_value, is_reverse)) ||\n    (is_keydown && e.keyCode !== parseInt(event_value, 10))\n  ) {\n    return false;\n  }\n\n  el.hide(animation);\n})(e));\n              \n\n      Interactions.handleEvent(e, processed);\n    });\n  \n<\/script>\n"},"is_shot_mode":false,"version_data":[],"comment_id":0}

        Preview.setData(data)

    if(!Preview.isExport() && Preview.isMenuEnabled()) {
        Preview.createMainToolbar();
    }

    if(data.menu_enabled) {
        Preview.createSidebar({
            'show_comments': data.export_mode ? false : true,
            'show_back_widget': data.export_mode || !data.is_user_logged_in ? false : true
        });
    }

    if(data.versions) {
        if(data.versions.getLength() > 1) {
            Preview.createVersionSwitch(data.versions)
        }
    }

    Preview.comet = new dmsDPPreview_Comet();

    var uin = '';

    if(uin && document.getElement('#name-set')) {
        document.getElement('#name-set').dispose();
    }

    new Hash(Preview.mData.versions_html).each(function(pHTML, pId) {

      Preview.cachePage(pId,pHTML);
    })

    if(uri.get('fragment')) {
      Preview.loadPageVersion(uri.get('fragment').toInt());
    }
 		Preview.fixModals();
});

Array.implement({

    /**
     * Dziala identycznie jak each() z tym wyjatkiem, ze jesli callback zwroci FALSE
     * petla zostanie przerwana
     */
    breakEach: function(fn, bind){

        for (var i = 0, l = this.length; i < l; i++) {

            if(false === fn.call(bind, this[i], i, this)) {
                break;
            }
        }
    }

});
</script>
</html>