<%-- 
    Document   : postentryview
    Created on : 09 21, 14, 3:57:20 PM
    Author     : Winnie Versosa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
  <head>
    <title>Ztook - Post Entry</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Expires" content="0"/>
    
    <link rel="stylesheet" href="data/e_f_8_0_c335655102e89a6166592e50af15f667ff71.css" type="text/css"/>    
    <script type="text/javascript"  src="data/4_b_d_1_f8b8cfb5259b09637d4b431e925a92895d77.js"></script>  
          
</head><body class="preview html-export">

      
<script type="text/javascript"  src="data/c_d_6_5_907e6b001fe29cdb8820cdbbbc686ff1119a.js"></script>
<link rel="stylesheet" href="data/3_8_b_9_bff27640c8d9755814c1392ff4e32e7bb40f.css" type="text/css"/>
<script>
  var Preview = new dmsDPPreview_Preview();
</script>

<div id="canvas">
    <div id="canvas-wrapper">
        <div id="canvas-area">
            <div id="main1">
                <div class="ElTextElement " style="z-index:10001;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:15px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:43px;position:absolute;" id="el3317655_748af80fc7c9c9f4d197422f1cf207e8" ><p class="wysiwyg-font-type-Cabin"><span style="font-weight: bold;">Spotted! </span>{post.ztookeename} is at {post.location} attending {post.event}</p></div>
    <script>(function() {Preview.mElements.set(3317655, document.getElementById('el3317655_748af80fc7c9c9f4d197422f1cf207e8'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script><textarea  style="z-index:10002;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(255,255,255,0);color:#474747;font-size:14px;font-family:Arial;text-align:left;background-image:url('');font-weight:regular;line-height:normal;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;box-shadow:;-o-box-shadow:;-moz-box-shadow:;-webkit-box-shadow:;left:25px;top:65px;disabled:;width:335px;height:100px;position:absolute;" id="el754224_95e8385dbf98efa86d54427f55c68c33" class="ElTextarea " ></textarea><script>(function() {Preview.mElements.set(754224, document.getElementById('el754224_95e8385dbf98efa86d54427f55c68c33'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
      <div class="ElImageWrapper " id="el4997273_9070913941971be87d14e1eb3f273386" style="z-index:10003;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:25px;top:173px;width:335px;height:382px;position:absolute;" >
                      <img class="ElImage " src="data/imagepixel.png" />
                </div>

    <script>(function() {Preview.mElements.set(4997273, document.getElementById('el4997273_9070913941971be87d14e1eb3f273386'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>    <div class="ElTextElement " style="z-index:10004;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:560px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:19px;position:absolute;" id="el4952186_06f7d26e769142615f2080e2099292f3" ><p class="wysiwyg-font-type-Cabin" style="font-size: 11px;"><span style="color: rgb(135, 135, 135);">posted by {post.postedBy} on {post.date}</span></p></div>
    <script>(function() {Preview.mElements.set(4952186, document.getElementById('el4952186_06f7d26e769142615f2080e2099292f3'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script><div id="el5146437_40520f195f4e82f7307263dfcdd2f69a" class="IconSVG " style="original_width:256 ;original_height:256 
;z-index:10005;color:#00a800;rotation:0;current_element_category_id:20973;left:325px;top:15px;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;" >
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" xml:space="preserve"><path fill='#00a800' fill="#343434" d="M128.09,0c17.712,0,34.36,3.333,49.785,10.032c15.489,6.666,29.083,15.783,40.718,27.384 c11.633,11.568,20.782,25.13,27.383,40.685c6.601,15.522,9.935,32.188,9.935,49.899s-3.334,34.377-9.935,49.899 c-6.601,15.521-15.75,29.116-27.383,40.685c-11.635,11.601-25.229,20.718-40.718,27.384C162.45,252.634,145.802,256,128.09,256 c-17.744,0-34.377-3.366-49.899-10.032c-15.555-6.666-29.117-15.783-40.685-27.384c-11.601-11.568-20.718-25.163-27.384-40.685 C3.423,162.377,0.09,145.712,0.09,128s3.333-34.377,10.032-49.899c6.667-15.555,15.784-29.116,27.384-40.685 c11.568-11.601,25.129-20.718,40.685-27.384C93.712,3.333,110.346,0,128.09,0z M215.259,104.439c1.438-1.47,2.223-3.3,2.288-5.424 c0.064-2.157-0.72-3.954-2.288-5.457l-16.666-17.124c-1.7-1.503-3.595-2.255-5.686-2.255c-2.092,0-3.922,0.751-5.556,2.255 l-72.398,72.545c-1.503,1.503-3.3,2.254-5.36,2.254c-2.091,0-3.921-0.751-5.522-2.254l-35.423-35.391 c-1.503-1.503-3.268-2.255-5.359-2.255c-2.092,0-4.02,0.752-5.85,2.255l-16.666,16.96c-1.503,1.503-2.222,3.334-2.222,5.457 c0,2.124,0.719,3.954,2.222,5.457l51.109,51.109c1.503,1.471,3.529,2.777,6.078,3.921c2.581,1.111,4.934,1.667,7.058,1.667h8.987 c2.124,0,4.444-0.523,6.96-1.601c2.516-1.079,4.575-2.419,6.176-3.987L215.259,104.439L215.259,104.439z" class="shape" style="-webkit-transform: scale(0.125, 0.125);-moz-transform: scale(0.125, 0.125);transform: scale(0.125, 0.125);"></path></svg></body></html>

</div><script>(function() {Preview.mElements.set(5146437, document.getElementById('el5146437_40520f195f4e82f7307263dfcdd2f69a'), {"interactive":false,"in_group":false,"in_component":false,"id_component":false})})();</script>
<script>
Interactions.ori_styles = {"3317655":{"opacity":null,"width":300,"height":43,"top":15,"left":25},"754224":{"opacity":null,"width":335,"height":100,"top":65,"left":25},"4997273":{"opacity":null,"width":335,"height":382,"top":173,"left":25},"4952186":{"opacity":null,"width":300,"height":19,"top":560,"left":25},"5146437":{"opacity":null,"width":32,"height":32,"top":15,"left":325,"original_width":"256 ","original_height":"256 \n"}};
Interactions.scrollable_el = document.getElementById('');


</script>

            </div>

            <div id="comments-pins"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
var uri = new URI(document.URL);
Preview.attachEvents();

window.addEvent('domready', function() {
    var data = {"id_project":"1170098","id_collection":"237370","id_page":"11813076","id_document":null,"version_of":null,"project_hash":"1ef043e343fde09296497ec728793809362292cc","active_collaboration":false,"menu_enabled":false,"export_mode":true,"add_comments":false,"page_base_url":null,"pages":[],"comments":null,"is_user_logged_in":false,"versions":"{\"5\":\"11813076\"}","current_version":"5","current_map":[],"versions_html":{"11813076":"    <div class=\"ElTextElement \" style=\"z-index:10001;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:15px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:43px;position:absolute;\" id=\"lblCaption\" ><p class=\"wysiwyg-font-type-Cabin\"><span style=\"font-weight: bold;\">Spotted! <\/span>{post.ztookeename} is at {post.location} attending {post.event}<\/p><\/div>\n    <script>(function() {Preview.mElements.set(3317655, document.getElementById('lblCaption'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><textarea  style=\"z-index:10002;border-top-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#c8c8c8;border-style:solid;background-color:rgba(255,255,255,0);color:#474747;font-size:14px;font-family:Arial;text-align:left;background-image:url('');font-weight:regular;line-height:normal;padding-top:6px;padding-right:10px;padding-bottom:6px;padding-left:10px;box-shadow:;-o-box-shadow:;-moz-box-shadow:;-webkit-box-shadow:;left:25px;top:65px;disabled:;width:335px;height:100px;position:absolute;\" id=\"txtboxCaption\" class=\"ElTextarea \" ><\/textarea><script>(function() {Preview.mElements.set(754224, document.getElementById('txtboxCaption'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>\n      <div class=\"ElImageWrapper \" id=\"imgUpload\" style=\"z-index:10003;border-width:0px;border-top-left-radius:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;border-bottom-left-radius:0px;border-color:#888;border-style:solid;background-color:none;left:25px;top:173px;width:335px;height:382px;position:absolute;\" >\n                      <img class=\"ElImage \" src=\"data\/imagepixel.png\" \/>\n                <\/div>\n\n    <script>(function() {Preview.mElements.set(4997273, document.getElementById('imgUpload'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>    <div class=\"ElTextElement \" style=\"z-index:10004;overflow:visible;color:;text-align:left;line-height:normal;font-weight:normal;font-style:normal;left:25px;top:560px;font-family:Arial;border-width:0px;border-style:solid;width:300px;height:19px;position:absolute;\" id=\"el4952186_8dc7515e094cc386af06738805d3f4ce\" ><p class=\"wysiwyg-font-type-Cabin\" style=\"font-size: 11px;\"><span style=\"color: rgb(135, 135, 135);\">posted by {post.postedBy} on {post.date}<\/span><\/p><\/div>\n    <script>(function() {Preview.mElements.set(4952186, document.getElementById('el4952186_8dc7515e094cc386af06738805d3f4ce'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script><div id=\"el5146437_21c27d459e2bb68ab5b8353b2704ed17\" class=\"IconSVG \" style=\"original_width:256 ;original_height:256 \n;z-index:10005;color:#00a800;rotation:0;current_element_category_id:20973;left:325px;top:15px;font-family:;border-width:0px;border-style:solid;width:32px;height:32px;position:absolute;\" >\n<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><body><svg version=\"1.1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" width=\"100%\" height=\"100%\" xml:space=\"preserve\"><path fill='#00a800' fill=\"#343434\" d=\"M128.09,0c17.712,0,34.36,3.333,49.785,10.032c15.489,6.666,29.083,15.783,40.718,27.384 c11.633,11.568,20.782,25.13,27.383,40.685c6.601,15.522,9.935,32.188,9.935,49.899s-3.334,34.377-9.935,49.899 c-6.601,15.521-15.75,29.116-27.383,40.685c-11.635,11.601-25.229,20.718-40.718,27.384C162.45,252.634,145.802,256,128.09,256 c-17.744,0-34.377-3.366-49.899-10.032c-15.555-6.666-29.117-15.783-40.685-27.384c-11.601-11.568-20.718-25.163-27.384-40.685 C3.423,162.377,0.09,145.712,0.09,128s3.333-34.377,10.032-49.899c6.667-15.555,15.784-29.116,27.384-40.685 c11.568-11.601,25.129-20.718,40.685-27.384C93.712,3.333,110.346,0,128.09,0z M215.259,104.439c1.438-1.47,2.223-3.3,2.288-5.424 c0.064-2.157-0.72-3.954-2.288-5.457l-16.666-17.124c-1.7-1.503-3.595-2.255-5.686-2.255c-2.092,0-3.922,0.751-5.556,2.255 l-72.398,72.545c-1.503,1.503-3.3,2.254-5.36,2.254c-2.091,0-3.921-0.751-5.522-2.254l-35.423-35.391 c-1.503-1.503-3.268-2.255-5.359-2.255c-2.092,0-4.02,0.752-5.85,2.255l-16.666,16.96c-1.503,1.503-2.222,3.334-2.222,5.457 c0,2.124,0.719,3.954,2.222,5.457l51.109,51.109c1.503,1.471,3.529,2.777,6.078,3.921c2.581,1.111,4.934,1.667,7.058,1.667h8.987 c2.124,0,4.444-0.523,6.96-1.601c2.516-1.079,4.575-2.419,6.176-3.987L215.259,104.439L215.259,104.439z\" class=\"shape\" style=\"-webkit-transform: scale(0.125, 0.125);-moz-transform: scale(0.125, 0.125);transform: scale(0.125, 0.125);\"><\/path><\/svg><\/body><\/html>\n\n<\/div><script>(function() {Preview.mElements.set(5146437, document.getElementById('el5146437_21c27d459e2bb68ab5b8353b2704ed17'), {\"interactive\":false,\"in_group\":false,\"in_component\":false,\"id_component\":false})})();<\/script>\n<script>\nInteractions.ori_styles = {\"3317655\":{\"opacity\":null,\"width\":300,\"height\":43,\"top\":15,\"left\":25},\"754224\":{\"opacity\":null,\"width\":335,\"height\":100,\"top\":65,\"left\":25},\"4997273\":{\"opacity\":null,\"width\":335,\"height\":382,\"top\":173,\"left\":25},\"4952186\":{\"opacity\":null,\"width\":300,\"height\":19,\"top\":560,\"left\":25},\"5146437\":{\"opacity\":null,\"width\":32,\"height\":32,\"top\":15,\"left\":325,\"original_width\":\"256 \",\"original_height\":\"256 \\n\"}};\nInteractions.scrollable_el = document.getElementById('');\n\n\n<\/script>\n"},"is_shot_mode":false,"version_data":[],"comment_id":0}

        Preview.setData(data)

    if(!Preview.isExport() && Preview.isMenuEnabled()) {
        Preview.createMainToolbar();
    }

    if(data.menu_enabled) {
        Preview.createSidebar({
            'show_comments': data.export_mode ? false : true,
            'show_back_widget': data.export_mode || !data.is_user_logged_in ? false : true
        });
    }

    if(data.versions) {
        if(data.versions.getLength() > 1) {
            Preview.createVersionSwitch(data.versions)
        }
    }

    Preview.comet = new dmsDPPreview_Comet();

    var uin = '';

    if(uin && document.getElement('#name-set')) {
        document.getElement('#name-set').dispose();
    }

    new Hash(Preview.mData.versions_html).each(function(pHTML, pId) {

      Preview.cachePage(pId,pHTML);
    })

    if(uri.get('fragment')) {
      Preview.loadPageVersion(uri.get('fragment').toInt());
    }
 		Preview.fixModals();
});

Array.implement({

    /**
     * Dziala identycznie jak each() z tym wyjatkiem, ze jesli callback zwroci FALSE
     * petla zostanie przerwana
     */
    breakEach: function(fn, bind){

        for (var i = 0, l = this.length; i < l; i++) {

            if(false === fn.call(bind, this[i], i, this)) {
                break;
            }
        }
    }

});
</script>

  </body>
</html>