/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function readURL(input, imgId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#' + imgId).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function submitPostForm(formId) {
    $('#' + formId).submit();

}

